package ca.mcgill.sel.ram.provider.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.CreateChildCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.LayoutElement;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.TypedElement;
import ca.mcgill.sel.ram.impl.ContainerMapImpl;
import ca.mcgill.sel.ram.impl.ElementMapImpl;
import ca.mcgill.sel.ram.provider.RAMEditPlugin;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Helper class with convenient static methods for working with EMF objects.
 * 
 * @author mschoettle
 */
public final class RAMEditUtil {

    /**
     * Creates a new instance of {@link RAMEditUtil}.
     */
    private RAMEditUtil() {
        // suppress default constructor
    }

    /**
     * Returns the complete signature of the given {@link Operation} including parameters.
     * 
     * @param adapterFactory the {@link AdapterFactory} to use
     * @param operation the {@link Operation} a signature should be returned for
     * @param includeClassName whether the class name should be included in the signature (at the beginning)
     * @param includeReturnType whether the return type of the operation should be appended
     * @return the complete signature of the given {@link Operation}
     */
    public static String getOperationSignature(AdapterFactory adapterFactory, Operation operation,
            boolean includeClassName, boolean includeReturnType) {
        StringBuffer stringBuffer = new StringBuffer();

        stringBuffer.append(RAMEditPlugin.INSTANCE.getString("_UI_Operation_type"));
        stringBuffer.append(" ");

        if (includeClassName) {
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation.eContainer()));
            stringBuffer.append(".");
        }

        stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation));
        stringBuffer.append("(");

        for (Parameter parameter : operation.getParameters()) {
            // if it is not the first one, add a ","
            if (operation.getParameters().indexOf(parameter) > 0) {
                stringBuffer.append(", ");
            }

            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, parameter.getType()));
            stringBuffer.append(" ");
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, parameter));
        }

        stringBuffer.append(")");
        
        if (includeReturnType) {
            stringBuffer.append(" : ");
            stringBuffer.append(EMFEditUtil.getTextFor(adapterFactory, operation.getReturnType()));
        }

        return stringBuffer.toString();
    }

    /**
     * Filters the list of all operations to a list of callable operations on the given receiver.
     * The given collection is directly modified and all operations that cannot be called on the receiver are
     * removed from it.
     * 
     * @param operations the list of all operations found
     * @param aspect the current aspect
     * @param receiver the receiver the operation is called on
     */
    public static void filterCallableOperations(Collection<?> operations, Aspect aspect, Classifier receiver) {
        Set<Classifier> callReceivers = Collections.emptySet();

        if (receiver != null) {
            callReceivers = RAMModelUtil.collectClassifiersFor(aspect, receiver);
        }

        Set<COREModelComposition> modelCompositions = new HashSet<>();
        modelCompositions.addAll(COREModelUtil.collectModelExtensions(aspect));
        modelCompositions.addAll(COREModelUtil.collectAllModelReuses(aspect));
        Set<Operation> mappedFromOperations = new HashSet<Operation>();

        for (COREModelComposition modelComposition : modelCompositions) {
            Collection<ClassifierMapping> ccm = EcoreUtil.getObjectsByType(modelComposition.getCompositions(),
                    RamPackage.Literals.CLASSIFIER_MAPPING);
            for (ClassifierMapping mapping : ccm) {                
                if (callReceivers.contains(mapping.getFrom())) {
                    for (OperationMapping operationMapping : mapping.getOperationMappings()) {
                        mappedFromOperations.add(operationMapping.getFrom());
                    }
                }
            }
        }

        // Filter out all operations that are not part of the classifiers in the callReceivers list.
        // The call receivers are the given receiver itself, super types, mapped classes and extended classes.
        for (Iterator<?> iterator = operations.iterator(); iterator.hasNext(); ) {
            Operation value = (Operation) iterator.next();

            // null is also contained in the list
            if (value != null) {
                if (!callReceivers.contains(value.eContainer())
                        || mappedFromOperations.contains(value)) {
                    iterator.remove();
                }
            }
        }
    }

    /**
     * Collects all possible structural features.
     * Valid choices must be a {@link StructuralFeature} and be a structural feature of the
     * classifier that is represented by the lifeline (or any classifier that is the same classifier,
     * e.g., through inheritance, mapping etc.) a fragment is located on.
     * Also, local properties of the initial message are valid as well.
     * 
     * @param aspect the {@link Aspect} containing the given objects
     * @param lifeline the lifeline for whose represented classifier the structural features are of interest
     * @param initialMessage the message that is defined, which contains the local properties
     * @param currentValue the current structural feature that is set
     * @return the filtered list of choices
     */
    public static Collection<?> collectStructuralFeatures(Aspect aspect, Lifeline lifeline,
            Message initialMessage, StructuralFeature currentValue) {
        Collection<Object> result = new UniqueEList<Object>();
        result.add(null);

        if (lifeline == null || initialMessage == null) {
            return result;
        }

        /**
         * Build a list of structural features of all represented classes.
         */
        TypedElement representedType = lifeline.getRepresents();

        if (representedType != null && representedType.getType() != null
                && representedType.getType() instanceof Classifier) {
            Classifier type = (Classifier) representedType.getType();

            Set<Classifier> classifiers = RAMModelUtil.collectClassifiersFor(aspect, type);

            for (Classifier classifier : classifiers) {
                TreeIterator<EObject> contents = EcoreUtil.getAllProperContents(classifier, true);

                while (contents.hasNext()) {
                    Object next = contents.next();

                    if (next instanceof StructuralFeature) {
                        result.add(next);
                    }
                }

            }
        }
        
        for (TemporaryProperty property : initialMessage.getLocalProperties()) {
            result.add(property);
        }
        
        Set<COREModelExtension> modelExtensions = COREModelUtil.collectModelExtensions(aspect);
        COREModelUtil.filterMappedElements(result, modelExtensions);

        return result;
    }
    
    /**
     * Creates a new command which adds a new entry to the layout map for a given container.
     *
     * @param editingDomain the {@link EditingDomain}
     * @param container the container of all layouted elements
     * @param object the object that should be layouted
     * @param layoutElement the {@link LayoutElement} for the object
     * @return the command which adds a new entry to the layout map, null if no layout or container map exists 
     */
    public static Command createAddLayoutElementCommand(EditingDomain editingDomain, EObject container, EObject object,
            LayoutElement layoutElement) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(container, RamPackage.Literals.ASPECT);

        if (aspect.getLayout() != null) {
            // Get the ContainerMap.
            ContainerMapImpl containerMap = EMFModelUtil.getEntryFromMap(aspect.getLayout().getContainers(), container);
            
            if (containerMap != null) {
                // Get the new child descriptor for the entry of the map.
                CommandParameter newChildDescriptor =
                        EMFEditUtil.getNewChildDescriptor(containerMap, RamPackage.Literals.CONTAINER_MAP__VALUE);
        
                // Set the key and value of the ElementMap.
                // We evade using commands here because it is not necessary.
                ElementMapImpl elementMap = (ElementMapImpl) newChildDescriptor.getValue();
        
                elementMap.setKey(object);
                elementMap.setValue(layoutElement);
        
                // The newChildDescriptor references the element map we modified, so we can just use
                // the CreateChildCommand.
                return CreateChildCommand.create(editingDomain, containerMap, newChildDescriptor,
                                                    Collections.EMPTY_LIST);
            }
        }
        
        return null;
    }

}
