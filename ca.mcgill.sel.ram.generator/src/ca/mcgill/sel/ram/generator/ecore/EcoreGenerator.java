package ca.mcgill.sel.ram.generator.ecore;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Attribute;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ReferenceType;
import ca.mcgill.sel.ram.Type;

/**
 * The generator of Ecore models from aspects (RAM).
 * Generates the equivalent of the RAM structural view to Ecore and saves it as .ecore file.
 * 
 * @author mschoettle
 */
public class EcoreGenerator {
    
    private static final EcoreFactory FACTORY = EcoreFactory.eINSTANCE;
    
    private Map<Classifier, EClassifier> classifierToEClassifierMap;
    
    private File targetFile;
    private Aspect aspect;
    
    /**
     * Creates a new Ecore Generator for the given aspect.
     * The resulting Ecore model will be saved to the given target file. 
     * 
     * @param aspect the source {@link Aspect} 
     * @param targetFile the file to which the Ecore model should be serialized
     */
    public EcoreGenerator(Aspect aspect, File targetFile) {
        classifierToEClassifierMap = new HashMap<>();
        
        this.targetFile = targetFile;
        this.aspect = aspect;
    }
    
    /**
     * Generates the Ecore model and saves the result.
     * 
     * @throws IOException in case of errors during saving
     */
    public void doGenerate() throws IOException {
        // TODO: Find better way for namespace name and URI.
        EPackage ePackage = createEPackage(aspect.getName(), "http://" + aspect.getName());
        
        Collection<REnum> enums =
                EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(), RamPackage.Literals.RENUM);
        
        for (REnum renum : enums) {
            EEnum eEnum = createEEnum(renum);
            
            ePackage.getEClassifiers().add(eEnum);
            classifierToEClassifierMap.put(renum, eEnum);
        }
        
        for (Classifier classifier : aspect.getStructuralView().getClasses()) {
            EClass eClass = null;
            
            if (classifier instanceof Class) {
                Class clazz = (Class) classifier;
                
                eClass = createEClass(clazz);
                ePackage.getEClassifiers().add(eClass);     
                classifierToEClassifierMap.put(clazz, eClass);
                
                for (Attribute attribute : clazz.getAttributes()) {
                    EAttribute eAttribute = createEAttribute(attribute);
                    eClass.getEStructuralFeatures().add(eAttribute);
                }
            } else {
                System.out.println("Unsupported: " + classifier);
            }
        }
        
        Map<AssociationEnd, EReference> associationToReferenceMap = new HashMap<>();
        
        for (Entry<Classifier, EClassifier> entry : classifierToEClassifierMap.entrySet()) {
            Classifier classifier = entry.getKey();
            EClassifier eClassifier = entry.getValue();
            
            if (eClassifier.eClass() == EcorePackage.Literals.ECLASS) {
                EClass eClass = (EClass) eClassifier;
                
                for (Classifier superType : classifier.getSuperTypes()) {
                    EClass superEClass = (EClass) classifierToEClassifierMap.get(superType);
                    eClass.getESuperTypes().add(superEClass);
                }
                
                for (AssociationEnd end : classifier.getAssociationEnds()) {
                    if (end.isNavigable()) {
                        EReference reference = createEReference(end);
                        
                        associationToReferenceMap.put(end, reference);
                        eClass.getEStructuralFeatures().add(reference);
                    }
                }
            }
        }
        
        for (Entry<AssociationEnd, EReference> entry : associationToReferenceMap.entrySet()) {
            AssociationEnd end = entry.getKey();
            EReference reference = entry.getValue();
            
            if (end.getOppositeEnd().isNavigable()) {
                EReference oppositeReference = associationToReferenceMap.get(end.getOppositeEnd());
                
                reference.setEOpposite(oppositeReference);
                oppositeReference.setEOpposite(reference);
            }
        }
        
        save(ePackage);
    }
    
    /**
     * Creates the equivalent Ecore attribute with the same properties for the given attribute.
     * 
     * @param attribute the source {@link Attribute}
     * @return the {@link EAttribute} with the same properties
     */
    private EAttribute createEAttribute(Attribute attribute) {
        EAttribute eAttribute = FACTORY.createEAttribute();
        eAttribute.setName(attribute.getName());
        eAttribute.setEType(getEType(attribute.getType()));
        
        return eAttribute;
    }
    
    /**
     * Creates the equivalent Ecore class with the same properties of the given classifier.
     * 
     * @param classifier the source {@link Class}
     * @return the {@link EClass}
     */
    private EClass createEClass(Class classifier) {
        EClass eClass = FACTORY.createEClass();
        eClass.setName(classifier.getName());
        eClass.setAbstract(classifier.isAbstract());
        
        return eClass;
    }
    
    /**
     * Creates the equivalent Ecore enum with the same properties of the given enum.
     * 
     * @param renum the source {@link REnum}
     * @return the {@link EEnum}
     */
    private EEnum createEEnum(REnum renum) {
        EEnum eEnum = FACTORY.createEEnum();
        eEnum.setName(renum.getName());
        
        for (REnumLiteral literal : renum.getLiterals()) {
            EEnumLiteral eLiteral = FACTORY.createEEnumLiteral();
            eLiteral.setName(literal.getName());
            eLiteral.setValue(renum.getLiterals().indexOf(literal));
            
            eEnum.getELiterals().add(eLiteral);
        }
        
        return eEnum;
    }

    /**
     * Creates an Ecore package with the name and URI.
     *  
     * @param name the name and namespace prefix of the package
     * @param nsURI the namespace URI of the package
     * @return the {@link EPackage}
     */
    private EPackage createEPackage(String name, String nsURI) {
        EPackage ePackage = FACTORY.createEPackage();
        ePackage.setName(name);
        ePackage.setNsPrefix(name);
        ePackage.setNsURI(nsURI);
        
        return ePackage;
    }
    
    /**
     * Creates the equivalent Ecore reference with the same properties of the given association end.
     * 
     * @param end the source {@link AssociationEnd}
     * @return the {@link EReference}
     */
    private EReference createEReference(AssociationEnd end) {
        EReference reference = FACTORY.createEReference();
        reference.setContainment(end.getReferenceType() == ReferenceType.COMPOSITION);
        reference.setName(end.getName());
        reference.setLowerBound(end.getLowerBound());
        reference.setUpperBound(end.getUpperBound());
        
        // TODO: Woven models have association woven in, need to handle this in a special way?
        // E.g., A --> ArrayList --> B in woven model, instead of A --> B in user model 
        Type referenceType = end.getType();
        reference.setEType(getEType(referenceType));
        
        // TODO: Add ordered and unique?
        
        return reference;
    }

    /**
     * Returns the corresponding Ecore classifier for the given RAM type.
     * 
     * @param type the {@link Type}
     * @return the corresponding Ecore classifier, null if none found
     */
    private EClassifier getEType(Type type) {
        EClassifier result = null;
        
        if (type instanceof PrimitiveType) {
            if (type instanceof REnum) {
                result = classifierToEClassifierMap.get(type);
            } else {
                switch (type.eClass().getClassifierID()) {
                    case RamPackage.RBOOLEAN:
                        result = EcorePackage.eINSTANCE.getEBoolean();
                        break;
                    case RamPackage.RCHAR:
                        result = EcorePackage.eINSTANCE.getEChar();
                        break;
                    case RamPackage.RDOUBLE:
                        result = EcorePackage.eINSTANCE.getEDouble();
                        break;
                    case RamPackage.RFLOAT:
                        result = EcorePackage.eINSTANCE.getEFloat();
                        break;
                    case RamPackage.RINT:
                        result = EcorePackage.eINSTANCE.getEInt();
                        break;
                    case RamPackage.RLONG:
                        result = EcorePackage.eINSTANCE.getELong();
                        break;
                    case RamPackage.RSTRING:
                        result = EcorePackage.eINSTANCE.getEString();
                        break;
                }
            }
        } else {
            result = classifierToEClassifierMap.get(type);
        }
        
        return result;
    }

    /**
     * Saves the package.
     * 
     * @param ePackage the Ecore package to save
     * @throws IOException if an error occurs during serialization
     */
    private void save(EPackage ePackage) throws IOException {
        ResourceSetImpl resourceSet = new ResourceSetImpl();
        // Need to use absolute path.
        Resource resource = resourceSet.createResource(URI.createFileURI(targetFile.getAbsolutePath()));
    
        // Add the resources to the resource to be saved.
        resource.getContents().add(ePackage);
    
        // Now save the content.
        resource.save(Collections.EMPTY_MAP);
    }

}
