package ca.mcgill.sel.ram.generator.common;

import java.util.ArrayList;
import java.util.List;

import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;

/**
 * Common helpers for the code generator.
 * @author tdimeco
 * @author mschoettle
 */
public class CommonHelpers {
    
    /**
     * Returns the fragment container of the fragment and all its fragments containers up the hierarchy.
     * E.g., a fragment located inside an operand is indirectly contained by 
     * the interaction (the container of the combined fragment) as well.
     * The list is sorted in increasing order of occurrence within the message view.
     * 
     * @param interaction the interaction containing all fragments (directly and indirectly)
     * @return a flattened list of all contained fragments
     */
    public List<InteractionFragment> getFlattenedFragments(Interaction interaction) {
        List<InteractionFragment> result = new ArrayList<>();
        
        flattenFragments(result, interaction);
        
        return result;
    }
    
    private List<InteractionFragment> flattenFragments(List<InteractionFragment> fragments, FragmentContainer container) {        
        for (InteractionFragment fragment : container.getFragments()) {
            fragments.add(fragment);
            
            if (fragment instanceof CombinedFragment) {
                CombinedFragment combinedFragment = (CombinedFragment) fragment;
                
                for (InteractionOperand operand : combinedFragment.getOperands()) {
                    flattenFragments(fragments, operand);
                }
            }
        }
        
        return fragments;
    }
    
}
