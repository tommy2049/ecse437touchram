package ca.mcgill.sel.ram.controller.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.AspectMessageView;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.InteractionOperand;
import ca.mcgill.sel.ram.Lifeline;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.util.MessageViewUtil;

/**
 * Helper class with convenient methods to test message views.
 * 
 * @author mschoettle
 */
public final class MessageViewTestUtil {
    
    /**
     * Creates a new instance.
     */
    private MessageViewTestUtil() {
        
    }
    
    /**
     * Returns the lifeline with the given name.
     * Requires that only one lifeline exists with this name, otherwise an assertion error occurs.
     * 
     * @param owner the {@link Interaction} containing the lifelines
     * @param name the name of the lifeline
     * @return the lifeline with the given name
     */
    public static Lifeline getLifelineByName(Interaction owner, String name) {
        Lifeline result = null;
        
        for (Lifeline lifeline : owner.getLifelines()) {
            if (name.equals(lifeline.getRepresents().getName())) {
                assertThat(result).as("More than one lifeline with name '%s' exists", name).isNull();
                
                result = lifeline;
            }
        }
        
        assertThat(result).as("Lifeline with name '%s' not found", name).isNotNull();
        
        return result;
    }
    
    /**
     * Returns the classifier with the given name.
     * Requires that only one classifier exists with this name, otherwise an assertion error occurs.
     * 
     * @param owner the {@link Aspect} containing the classifiers
     * @param name the name of the classifier
     * @return the classifier with the given name
     */
    public static Classifier getClassifierByName(Aspect owner, String name) {
        Classifier result = null;
        
        for (Classifier classifier : owner.getStructuralView().getClasses()) {
            if (name.equals(classifier.getName())) {
                assertThat(result).as("More than one classifier with name '%s' exists", name).isNull();
                
                result = classifier;
            }
        }
        
        assertThat(result).as("Classifier with name '%s' not found", name).isNotNull();
        
        return result;
    }
    
    /**
     * Returns the operation with the given name.
     * Requires that only one operation exists with this name, otherwise an assertion error occurs.
     * 
     * @param owner the {@link Classifier} containing the operation
     * @param name the name of the operation
     * @return the operation with the given name
     */
    public static Operation getOperationByName(Classifier owner, String name) {
        Operation result = null;
        
        for (Operation operation : owner.getOperations()) {
            if (name.equals(operation.getName())) {
                assertThat(result).as("More than one operation with name '%s' exists", name).isNull();
                
                result = operation;
            }
        }
        
        assertThat(result).as("Operation with name '%s' of classifier '%s' not found", name, owner.getName())
                          .isNotNull();
        
        return result;
    }
    
    /**
     * Returns the message view defining the operation of the classifier.
     * 
     * @param aspect the {@link Aspect} containing the message view
     * @param className the name of the class
     * @param operationName the name of the operation
     * @return the message view defining the operation
     */
    public static MessageView loadMessageView(Aspect aspect, String className, String operationName) {
        Classifier classifier = MessageViewTestUtil.getClassifierByName(aspect, className);
        Operation operation = MessageViewTestUtil.getOperationByName(classifier, operationName);
        
        MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, operation);
        assertThat(messageView).isNotNull();
        
        return messageView;
    }
    
    /**
     * Returns the set of affected messages.
     * I.e., a message is considered affected, if a given fragment is part of a message.
     * The affected messages are collected recursively, which means that {@link CombinedFragment} contained in the
     * list of fragments are further examined to find all messages located within.
     * 
     * @param fragments a list of all fragments to consider
     * @return the set of affected messages
     */
    public static Set<Message> getAffectedMessages(List<InteractionFragment> fragments) {
        Set<Message> result = new HashSet<>();
        
        for (InteractionFragment fragment : fragments) {
            if (fragment instanceof MessageOccurrenceSpecification) {
                MessageOccurrenceSpecification messageEnd = (MessageOccurrenceSpecification) fragment;
                result.add(messageEnd.getMessage());
            } else if (fragment instanceof CombinedFragment) {
                // I'd argue that the tester has to add them specifically, 
                // otherwise there is no control over what exactly should be tested.
                CombinedFragment combinedFragment = (CombinedFragment) fragment;
                
                for (InteractionOperand operand : combinedFragment.getOperands()) {
                    Set<Message> nestedMessages = getAffectedMessages(operand.getFragments());
                    result.addAll(nestedMessages);
                }
            }
        }
        
        return result;
    }
    
    /**
     * Returns the aspect message view that advices the given operation.
     * Requires that there is only one aspect message view that advices the operation.
     * 
     * @param aspect the aspect the aspect message view is contained in
     * @param operation the operation that is adviced
     * @return the aspect message view that advices the operation, null if none found
     */
    public static AspectMessageView getAspectMessageView(Aspect aspect, Operation operation) {
        List<AspectMessageView> aspectMessageViews =
                MessageViewUtil.getMessageViewsOfType(aspect, AspectMessageView.class);
        assertThat(aspectMessageViews).as("No AspectMessageView found")
                                      .isNotEmpty();
        
        assertThat(aspectMessageViews).extracting("pointcut")
                                      .as("More than one AspectMessageView for the same Operation.")
                                      .containsOnlyOnce(operation);
        
        for (AspectMessageView amv : aspectMessageViews) {
            if (amv.getPointcut() == operation) {
                return amv;
            }
        }
        
        fail("No AspectMessageView found.");
        
        return null;
    }

}
