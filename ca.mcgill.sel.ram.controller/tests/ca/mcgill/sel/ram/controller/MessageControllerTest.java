package ca.mcgill.sel.ram.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.junit.Rule;
import org.junit.Test;

import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.CombinedFragment;
import ca.mcgill.sel.ram.FragmentContainer;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageEnd;
import ca.mcgill.sel.ram.MessageOccurrenceSpecification;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.StructuralFeature;
import ca.mcgill.sel.ram.TemporaryProperty;
import ca.mcgill.sel.ram.ValueSpecification;
import ca.mcgill.sel.ram.controller.util.MessageViewTestUtil;
import ca.mcgill.sel.ram.controller.util.ModelResource;
import ca.mcgill.sel.ram.controller.util.TestUtil;

public class MessageControllerTest {

    @Rule
    public final ModelResource modelResource = new ModelResource("tests/models/MessageViews/Messages.ram") {

        @Override
        public void modelLoaded(EObject model) {
            aspect = (Aspect) model;
        }

    };

    private static MessageController controller = ControllerFactory.INSTANCE.getMessageController();

    private Aspect aspect;
    private MessageView messageView;
    private Interaction interaction;

    private void loadMessageView(String className, String operationName) {
        messageView = MessageViewTestUtil.loadMessageView(aspect, className, operationName);
        interaction = messageView.getSpecification();
    }

    @Test
    public void testMoveMessage_FragmentBefore_MovedUp() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 2;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index - 1);

        controller.moveMessage(message, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_FragmentAfter_MovedDown() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 2;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index + 1);

        controller.moveMessage(message, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_AnotherMessageBefore_MovedUp() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 15;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index - 2);

        controller.moveMessage(message, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_AnotherMessageAfter_MovedDown() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 13;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index + 2);

        controller.moveMessage(message, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_FirstMessage_NothingMoved() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 2;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        // Move message above fragment first.
        controller.moveMessage(message, true);

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        controller.moveMessage(message, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_LastMessage_NothingMoved() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 15;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());

        controller.moveMessage(message, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_MessageInNestedBehaviour_NothingMoved() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 9;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());

        controller.moveMessage(message, false);
        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);

        // Move up above the other fragment.
        controller.moveMessage(message, true);
        interactionFragments = new ArrayList<>(interaction.getFragments());

        controller.moveMessage(message, true);
        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_FirstMessageInOperand_MovedDown() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 0;

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(4);
        FragmentContainer container = combinedFragment.getOperands().get(0);
        MessageEnd sendEvent = (MessageEnd) container.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(container.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index + 2);

        controller.moveMessage(message, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_LastMessageInOperand_MovedUp() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 2;

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(4);
        FragmentContainer container = combinedFragment.getOperands().get(0);
        MessageEnd sendEvent = (MessageEnd) container.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(container.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index - 2);

        controller.moveMessage(message, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_MessageInOperand_NothingMoved() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 0;

        CombinedFragment combinedFragment = (CombinedFragment) interaction.getFragments().get(4);
        FragmentContainer container = combinedFragment.getOperands().get(0);
        MessageEnd sendEvent = (MessageEnd) container.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(container.getFragments());

        // Make sure it's not moved when it is the first message.
        controller.moveMessage(message, true);
        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), interactionFragments);

        // Move down after the other message.
        controller.moveMessage(message, false);
        interactionFragments = new ArrayList<>(container.getFragments());

        // Make sure it's not moved when it is the last message.
        controller.moveMessage(message, false);
        TestUtil.assertContainsOnlyElementsExactlyOf(container.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_NestedMessageBefore_MovedUp() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 13;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index - 7);

        controller.moveMessage(message, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_NestedMessageAfter_MovedDown() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 2;

        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        // Move message down first in front of message with nested behaviour.
        controller.moveMessage(message, false);
        controller.moveMessage(message, false);

        index = interaction.getFragments().indexOf(sendEvent);
        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index + 7);

        controller.moveMessage(message, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    @Test
    public void testMoveMessage_NestedMessage_AllFragmentsMoved() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 6;

        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 7), index + 2);

        controller.moveMessage(message, false);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }
    
    @Test
    public void testMoveMessage_ImplementationClassCalled_NoNestedBehaviour() {
        loadMessageView("Caller", "testMessageMoving");
        int index = 17;
        
        MessageEnd sendEvent = (MessageEnd) interaction.getFragments().get(index);
        Message message = sendEvent.getMessage();

        List<InteractionFragment> interactionFragments = new ArrayList<>(interaction.getFragments());
        move(interactionFragments, interactionFragments.subList(index, index + 2), index - 2);

        controller.moveMessage(message, true);

        TestUtil.assertContainsOnlyElementsExactlyOf(interaction.getFragments(), interactionFragments);
    }

    private void move(List<InteractionFragment> fragments, List<InteractionFragment> subList, int toIndex) {
        List<InteractionFragment> fragmentsToMove = new ArrayList<>(subList);
        fragments.removeAll(subList);
        fragments.addAll(toIndex, fragmentsToMove);
    }
    
    @Test
    public void testChangeAssignTo_TemporaryPropertyUsedElseWhere_TemporaryPropertyNotRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(2);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        MessageOccurrenceSpecification otherEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        StructuralFeature newAssignTo = otherEvent.getMessage().getAssignTo();
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.changeAssignTo(message, newAssignTo);
        
        assertThat(message.getAssignTo()).isEqualTo(newAssignTo);
        assertThat(assignTo.eContainer()).isNotNull();
    }
    
    @Test
    public void testChangeAssignTo_TemporaryPropertyUnused_TemporaryPropertyRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        MessageOccurrenceSpecification otherEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(2);
        StructuralFeature newAssignTo = otherEvent.getMessage().getAssignTo();
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.changeAssignTo(message, newAssignTo);
        
        assertThat(message.getAssignTo()).isEqualTo(newAssignTo);
        assertThat(assignTo.eContainer()).isNull();
    }
    
    @Test
    public void testCreateTemporaryAssignment_TemporaryPropertyUsedElseWhere_TemporaryPropertyNotRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(2);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.createTemporaryAssignment(message);
        
        assertThat(message.getAssignTo()).isNotEqualTo(assignTo);
        assertThat(assignTo.eContainer()).isNotNull();
    }
    
    @Test
    public void testCreateTemporaryAssignment_TemporaryPropertyUnused_TemporaryPropertyRemoved() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        // Change assignTo where the old assign to is still assigned to somewhere else.
        controller.createTemporaryAssignment(message);
        
        assertThat(message.getAssignTo()).isNotEqualTo(assignTo);
        assertThat(assignTo.eContainer()).isNull();
    }
    
    @Test
    public void testChangeAssignTo_RemoveAssignment_AssignmentUnset() {
        loadMessageView("Caller", "testChangeAssignTo");
        
        MessageOccurrenceSpecification sendEvent = (MessageOccurrenceSpecification) interaction.getFragments().get(4);
        Message message = sendEvent.getMessage();
        StructuralFeature assignTo = message.getAssignTo();

        assertThat(assignTo).isNotNull();
        assertThat(assignTo).isInstanceOf(TemporaryProperty.class);
        
        // Remove assignTo where the property is not assigned to anywhere else.
        controller.changeAssignTo(message, null);
        
        assertThat(message.getAssignTo()).isNull();
        assertThat(assignTo.eContainer()).isNull();
    }

    @Test
    public void testChangeReturnValue_ReplyMessageWithReturnValue_ReturnValueChanged() {
        loadMessageView("Callee", "myMethod");

        Message replyMessage = interaction.getMessages().get(1);
        assertThat(replyMessage.getMessageSort()).isEqualTo(MessageSort.REPLY);
        
        ValueSpecification returns = replyMessage.getReturns();
        assertThat(returns).isNotNull();

        controller.changeReturnValue(replyMessage, null);

        assertThat(replyMessage.getReturns()).isNull();
        assertThat(returns.eContainer()).isNull();
    }

}
