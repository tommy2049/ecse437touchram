package ca.mcgill.sel.ram.controller;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREMapping;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.CoreFactory;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREConfigurationUtil;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Association;
import ca.mcgill.sel.ram.AssociationEnd;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ClassifierMapping;
import ca.mcgill.sel.ram.MessageView;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.OperationMapping;
import ca.mcgill.sel.ram.OperationType;
import ca.mcgill.sel.ram.Parameter;
import ca.mcgill.sel.ram.RAMVisibilityType;
import ca.mcgill.sel.ram.RInt;
import ca.mcgill.sel.ram.RamFactory;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.ReferenceType;
import ca.mcgill.sel.ram.util.Constants;
import ca.mcgill.sel.ram.util.MessageViewUtil;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * The controller for {@link Association}s and {@link AssociationEnd}s.
 *
 * @author mschoettle
 * @author cbensoussan
 */
public class AssociationController extends BaseController {

    /**
     * Creates a new instance of {@link AssociationController}.
     */
    protected AssociationController() {
        // Prevent anyone outside this package to instantiate.
    }

    /**
     * Sets the selected features of the given {@link AssociationEnd} according to the given set of features
     * and executes the command.
     *
     * @param aspect The current aspect
     * @param associationEnd the {@link AssociationEnd} the selected features should be changed for
     * @param reuseConfiguration The reuse configuration when the user reused the concern
     */
    public void setFeatureSelection(Aspect aspect, AssociationEnd associationEnd,
            COREReuseConfiguration reuseConfiguration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        CompoundCommand compoundCommand;
        if (oppositeEnd.isNavigable()) {
            compoundCommand = new BidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        } else {
            compoundCommand = new UnidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd,
                    reuseConfiguration);
            compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, associationEnd));
        }
        
        // Dirty fix to not have an empty compound command when executing
        compoundCommand.append(SetCommand.create(editingDomain, reuseConfiguration, 
                CorePackage.Literals.CORE_NAMED_ELEMENT__NAME, reuseConfiguration.getName()));
        
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Creates the feature selection, helper when called without selectedFeatures.
     * @param aspect The current aspect
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @return The compound command
     */
    public CompoundCommand createFeatureSelection(Aspect aspect, AssociationEnd associationEnd,
            AssociationEnd oppositeEnd) {
        return createFeatureSelection(aspect, associationEnd, oppositeEnd, null, null);
    }

    /**
     * Creates the feature selection.
     * @param aspect The current aspect
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param userSelectedConfiguration The selected features
     * @param oppositeSelectedConfiguration The selected configuration of the opposite end
     * @return The compound command
     */
    private CompoundCommand createFeatureSelection(Aspect aspect, AssociationEnd associationEnd,
            AssociationEnd oppositeEnd, COREReuseConfiguration userSelectedConfiguration,
            COREReuseConfiguration oppositeSelectedConfiguration) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();

        COREConcern concern = null;
        try {
            concern = COREModelUtil.getLocalConcern(Constants.ASSOCIATION_CONCERN_LOCATION,
                    aspect.getCoreConcern().eResource().getURI());
        } catch (IOException e) {
            return null;
        }

        HashSet<COREFeature> automaticSelectedFeatures = new HashSet<COREFeature>();
        if (userSelectedConfiguration != null) {
            automaticSelectedFeatures.addAll(userSelectedConfiguration.getSelected());
        }

        getSelectedFeatures(associationEnd, oppositeEnd, concern,
                automaticSelectedFeatures, oppositeSelectedConfiguration);
        
        // In case the features cannot be found, make sure that there is no invalid reuse created.
        if (!automaticSelectedFeatures.isEmpty()) {
//            COREModelReuse modelReuse = createModelReuse(aspect, associationEnd, oppositeEnd, concern,
//                    userSelectedConfiguration, automaticSelectedFeatures);
//    
//            Aspect wovenAspect = COREWeaverUtil.createWovenAspect(modelReuse.getReuse().getSelectedConfiguration(),
//                    concern, true, true);
//    
//            compoundCommand.append(AddCommand.create(editingDomain, aspect,
//                    CorePackage.Literals.CORE_MODEL__MODEL_REUSES, modelReuse));
//    
//            for (COREFeature feature : aspect.getRealizes()) {
//                compoundCommand.append(AddCommand.create(editingDomain, feature,
//                        CorePackage.Literals.CORE_FEATURE__REUSES, modelReuse.getReuse()));
//            }
//    
//            createReuseInstantiation(aspect, wovenAspect, associationEnd,
//                    modelReuse, compoundCommand);
//    
//            compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
//                    RamPackage.Literals.ASSOCIATION_END__FEATURE_SELECTION, modelReuse));
        }

        return compoundCommand;
    }

    /**
     * Creates a model reuse with a reuse and configurations given selected features.
     *
     * @param aspect The current aspect
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param concern The association concern
     * @param userSelectedConfiguration The configuration manually selected by the user
     * @param automaticallySelectedFeatures The already selected features
     *
     * @return Returns the created {@link COREModelReuse}
     */
    private COREModelReuse createModelReuse(Aspect aspect, AssociationEnd associationEnd, AssociationEnd oppositeEnd,
            COREConcern concern, COREReuseConfiguration userSelectedConfiguration,
            Set<COREFeature> automaticallySelectedFeatures) {

        // A configuration for the complete selection
        COREReuseConfiguration automaticConfiguration = CoreFactory.eINSTANCE.createCOREReuseConfiguration();
        automaticConfiguration.setName("Automatic selection");
        automaticConfiguration.getSelected().addAll(automaticallySelectedFeatures);
        automaticConfiguration.setSource(concern.getFeatureModel());

        COREReuse reuse = COREModelUtil.createReuse(aspect.getCoreConcern(), concern);
        automaticConfiguration.setReuse(reuse);

        if (userSelectedConfiguration != null) {
            automaticConfiguration.getReexposed().addAll(userSelectedConfiguration.getReexposed());
            userSelectedConfiguration.setName(Constants.USER_SELECTED_CONFIGURATION_NAME);
            // Ensure that the user selection is not contained in another reuse.
            // Otherwise, adding it will remove it from its original container and cause strange side effects
            // when dealing with commands, e.g., undoing will produce a different result.
            // See issue #472.
            COREReuseConfiguration copy = 
                    COREConfigurationUtil.getConfigurationCopy(userSelectedConfiguration, false, false);
            reuse.getConfigurations().add(copy);
        }

        reuse.getConfigurations().add(automaticConfiguration);
        reuse.setSelectedConfiguration(automaticConfiguration);
        reuse.setName(getReuseName(aspect, concern.getName(), associationEnd.getClassifier().getName(),
                oppositeEnd.getClassifier().getName()));

        COREModelReuse modelReuse = COREModelUtil.createModelReuse(null, reuse);
        
        return modelReuse;
    }

    /**
     * Gets the selected features.
     * @param associationEnd The association end
     * @param oppositeEnd The opposite end
     * @param concern The association concern
     * @param selectedFeatures The already selected features, if any
     * @param oppositeSelectedConfiguration The selected configuration of the opposite end
     */
    private void getSelectedFeatures(AssociationEnd associationEnd, AssociationEnd oppositeEnd, COREConcern concern,
            Set<COREFeature> selectedFeatures, COREReuseConfiguration oppositeSelectedConfiguration) {
        if (associationEnd.getUpperBound() == 1) {
            selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                    Constants.FEATURE_NAME_ONE));
        } else {
            selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                    Constants.FEATURE_NAME_MANY));
            if (associationEnd.getLowerBound() > 0 && !RAMModelUtil.hasKeyIndexedSelection(selectedFeatures)) {
                selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                        Constants.FEATURE_NAME_MINIMUM));
            }
            if (associationEnd.getUpperBound() > 1) {
                selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                        Constants.FEATURE_NAME_MAXIMUM));
            }
        }

        if (oppositeEnd.isNavigable()) {
            if (oppositeEnd.getUpperBound() == 1) {
                selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                        Constants.FEATURE_NAME_ONE_OPPOSITE));
            } else {
                if (oppositeSelectedConfiguration != null) {
                    // Opposite side updated its selected features and its reuse was already performed.
                    if (getFeatureByName(oppositeSelectedConfiguration.getSelected(),
                            Constants.FEATURE_NAME_ORDERED) != null) {
                        selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                                Constants.FEATURE_NAME_ORDERED_OPPOSITE));
                    } else if (getFeatureByName(oppositeSelectedConfiguration.getSelected(),
                            Constants.FEATURE_NAME_PLAIN) != null) {
                        selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                                Constants.FEATURE_NAME_PLAIN_OPPOSITE));
                    } else {
                        selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                                Constants.FEATURE_NAME_MANY_OPPOSITE));
                    }
                } else {
                    selectedFeatures.addAll(getSelectedFeatures(concern.getFeatureModel(),
                            Constants.FEATURE_NAME_MANY_OPPOSITE));
                }
            }
        }
    }

    /**
     * Create the core model reuse for the COREReuse.
     *
     * @param aspect The current aspect
     * @param externalAspect the woven aspect
     * @param associationEnd The association end for which a selection was done
     * @param modelReuse the model reuse to set the mappings for
     * @param compoundCommand The {@link CompoundCommand}
     */
    private void createReuseInstantiation(Aspect aspect, Aspect externalAspect, AssociationEnd associationEnd,
            COREModelReuse modelReuse, CompoundCommand compoundCommand) {

        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        Classifier associatedClassifier = null;
        Classifier dataClassifier = null;
        Classifier keyClassifier = null;

        for (Classifier classifier : externalAspect.getStructuralView().getClasses()) {
            if (classifier.getName().equals(Constants.ASSOCIATED_CLASS_NAME)
                    || classifier.getName().equals(Constants.VALUE_CLASS_NAME)) {
                associatedClassifier = classifier;
            } else if (Constants.DATA_CLASS_NAME.equals(classifier.getName())) {
                dataClassifier = classifier;
            } else if (Constants.KEY_CLASS_NAME.equals(classifier.getName())) {
                keyClassifier = classifier;
            }
        }

        modelReuse.setSource(externalAspect);

        ClassifierMapping associatedClassifierMapping = RamFactory.eINSTANCE.createClassifierMapping();
        associatedClassifierMapping.setFrom(associatedClassifier);
        associatedClassifierMapping.setTo(oppositeEnd.getClassifier());

        ClassifierMapping dataClassifierMapping = RamFactory.eINSTANCE.createClassifierMapping();
        dataClassifierMapping.setFrom(dataClassifier);
        dataClassifierMapping.setTo(associationEnd.getClassifier());

        if (keyClassifier != null) {
            ClassifierMapping keyClassifierMapping = RamFactory.eINSTANCE.createClassifierMapping();
            keyClassifierMapping.setFrom(keyClassifier);
            modelReuse.getCompositions().add(keyClassifierMapping);
        }

        modelReuse.getCompositions().add(associatedClassifierMapping);
        modelReuse.getCompositions().add(dataClassifierMapping);

        createOperationsMappings(aspect, associationEnd, dataClassifierMapping, compoundCommand);
        createOperationsMappings(aspect, oppositeEnd, associatedClassifierMapping, compoundCommand);

    }

    /**
     * Given a {@link ClassifierMapping}, it gets the operations of the from classifier and creates
     * a copy in the to classifier if it does not exist, otherwise it finds the existing one to map it.
     *
     * @param aspect The current aspect
     * @param associationEnd The association end for which a selection was done
     * @param classifierMapping The {@link ClassifierMapping} for which we need to create operation mappings
     * @param compoundCommand The {@link CompoundCommand}
     */
    private void createOperationsMappings(Aspect aspect, AssociationEnd associationEnd,
            ClassifierMapping classifierMapping, CompoundCommand compoundCommand) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(classifierMapping.getTo());
        for (Operation operation : classifierMapping.getFrom().getOperations()) {
            OperationMapping operationMapping = RamFactory.eINSTANCE.createOperationMapping();
            classifierMapping.getOperationMappings().add(operationMapping);

            operationMapping.setFrom(operation);
            Operation mapped = ControllerFactory.INSTANCE.getClassController().createOperationCopyWithoutCommand(
                    operationMapping, getOperationName(operation.getName(), associationEnd.getName(),
                            operation.getParameters()),
                    aspect.getStructuralView());

            if (COREVisibilityType.PUBLIC.equals(mapped.getVisibility())) {
                mapped.setVisibility(COREVisibilityType.CONCERN);
                mapped.setExtendedVisibility(RAMVisibilityType.PACKAGE);
            }

            if (RAMModelUtil.isUniqueOperation(classifierMapping.getTo(), 
                    mapped.getName(), mapped.getReturnType(), mapped.getParameters())) {
                if (mapped.getOperationType() == OperationType.CONSTRUCTOR) {
                    mapped.setVisibility(COREVisibilityType.PUBLIC);
                    compoundCommand.append(AddCommand.create(editingDomain, classifierMapping.getTo(),
                            RamPackage.Literals.CLASSIFIER__OPERATIONS, mapped, 0));
                } else {
                    compoundCommand.append(AddCommand.create(editingDomain, classifierMapping.getTo(),
                        RamPackage.Literals.CLASSIFIER__OPERATIONS, mapped));
                }
            } else {
                for (Operation existingOperation : classifierMapping.getTo().getOperations()) {
                    if (RAMModelUtil.hasSameSignature(mapped, existingOperation)) {
                        mapped = existingOperation;
                    }
                }
            }
            
            if (associationEnd.isStatic()) {
                compoundCommand.append(SetCommand.create(editingDomain, mapped, 
                        RamPackage.Literals.OPERATION__STATIC, true));
            }
            operationMapping.setTo(mapped);
        }
    }

    /**
     * Get the unique name of the reuse in the format Association_FromClass_ToClass[0-9]* .
     * @param aspect The current aspect
     * @param concernName The name of the reused concern
     * @param fromClassName The name of the FROM classifier
     * @param toClassName The name of the TO classifier
     * @return the unique name of the reuse
     */
    private String getReuseName(Aspect aspect, String concernName, String fromClassName, String toClassName) {
        // Get the list of other reuses (used to generate a unique name for the reuse)
        Set<COREReuse> reuses = new HashSet<COREReuse>();

        for (COREFeature feature : aspect.getRealizes()) {
            reuses.addAll(feature.getReuses());
        }

        String name = concernName + "_" + fromClassName + "_" + toClassName;
        return COREModelUtil.createUniqueNameFromElements(name, reuses);
    }

    /**
     * Gets the name of the mapped operation.
     * Given an association with a role name myObservers, the operation are generated as follow:
     * get --> getMyObserver (same for add, remove, contains and getKey)
     * size --> sizeOfMyObservers (same for keySet and values)
     * getAssociated --> getMyObservers
     *
     * @param operationName the name of the operation
     * @param associationEndName the name of the association end
     * @param parameters the parameters of the operation
     *
     * @return the name of the mapped operation
     */
    private String getOperationName(String operationName, String associationEndName, EList<Parameter> parameters) {
        String associationName = associationEndName.substring(0, 1).toUpperCase() + associationEndName.substring(1);
        String associationNameSingular = associationName;
//        if (associationName.endsWith("s")) {
//            associationNameSingular = associationNameSingular.substring(0, associationNameSingular.length() - 1);
//        }

        String mappedName = "";

        if ("getAssociated".equals(operationName)) {
            mappedName = "get" + associationName;
        } else if (operationName.matches("(get|add|remove|contains|put|getKey)")) {
            mappedName = operationName + associationNameSingular;
            if (parameters.size() > 0 && parameters.get(0).getType() instanceof RInt) {
                mappedName += "At";
            }
        } else if (operationName.matches("(size|keySet|values)")) {
            mappedName = operationName + "Of" + associationName;
        } else if ("create".equals(operationName)) {
            mappedName = operationName;
        } else {
            mappedName = operationName + associationName;
        }

        return mappedName;
    }

    /**
     * Returns a command that deletes the feature selection of the given {@link AssociationEnd},
     * deletes the mapped operations and the feature reuses from the features that realize the aspect.
     * Returns <code>null</code> if there is no feature selection associated with the {@link AssociationEnd}.
     *
     * @param command the overall {@link CompoundCommand} containing all to be executed commands
     * @param aspect The current aspect
     * @param associationEnd the {@link AssociationEnd} the selected features should be changed for
     *
     * @return the compound command, null if there is no feature selection associated with the association end
     */
    private CompoundCommand deleteFeatureSelection(CompoundCommand command, Aspect aspect,
                AssociationEnd associationEnd) {
        if (associationEnd.getFeatureSelection() != null) {
            EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
            CompoundCommand compoundCommand = new CompoundCommand();

            compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                    RamPackage.Literals.ASSOCIATION_END__FEATURE_SELECTION, SetCommand.UNSET_VALUE));

            COREModelReuse modelReuse = associationEnd.getFeatureSelection();
            compoundCommand.append(deleteMappedOperations(command, aspect, modelReuse,
                    associationEnd.getClassifier(), editingDomain));
            if (associationEnd.getClassifier() != associationEnd.getType()) {
                compoundCommand.append(deleteMappedOperations(command, aspect, modelReuse,
                        (Classifier) associationEnd.getType(), editingDomain));
            }

            compoundCommand.append(RemoveCommand.create(editingDomain,
                    associationEnd.getFeatureSelection().getReuse()));

            compoundCommand.append(RemoveCommand.create(editingDomain, associationEnd.getFeatureSelection()));

            return compoundCommand;
        }

        return null;
    }

    /**
     * Delete the mapped operations.
     *
     * @param command the overall {@link CompoundCommand} containing all to be executed commands
     * @param aspect The current aspect
     * @param modelReuse the modelReuse to look into for operations to delete
     * @param classifier The classifier for which to delete operations
     * @param editingDomain the {@link EditingDomain} to use for executing commands
     *
     * @return the created {@link CompoundCommand}
     */
    private CompoundCommand deleteMappedOperations(CompoundCommand command, Aspect aspect, COREModelReuse modelReuse,
            Classifier classifier, EditingDomain editingDomain) {
        CompoundCommand compoundCommand = new CompoundCommand();
        
        if (modelReuse != null) {
            for (COREModelElementComposition<?> composition : modelReuse.getCompositions()) {
                COREMapping<?> mapping = (COREMapping<?>) composition;
                
                if (classifier.equals(mapping.getTo())) {
                    ClassifierMapping classifierMapping = (ClassifierMapping) mapping;
                    for (OperationMapping operationMapping : classifierMapping.getOperationMappings()) {
                        Operation to = operationMapping.getTo();
                        
                        if (to.getOperationType() != OperationType.CONSTRUCTOR) {
                            MessageView messageView = MessageViewUtil.getMessageViewFor(aspect, to);
                            
                            if (messageView != null) {
                                compoundCommand.append(AspectController.createRemoveMessageViewCommand(messageView));
                            }
                        
                            /**
                             * Ensure that the operation is not mapped by any other operation mappings.
                             * Only if its only mapped by the current one (which will be deleted), 
                             * also delete the operation.
                             */
                            List<EObject> referencingObjects =
                                    EMFModelUtil.findCrossReferences(to, CorePackage.Literals.CORE_LINK__TO);
                            referencingObjects.remove(operationMapping);
    
                            Collection<COREMapping<COREModelElement>> mappings =
                                    EcoreUtil.getObjectsByType(command.getResult(), CorePackage.Literals.CORE_MAPPING);
                            referencingObjects.removeAll(mappings);
                            
                            if (referencingObjects.isEmpty()) {
                                compoundCommand.append(RemoveCommand.create(editingDomain, to));
                            }
                            
                            compoundCommand.append(RemoveCommand.create(editingDomain, operationMapping));
                        }
                    }
                }
            }
        }
        
        return (compoundCommand.isEmpty()) ? null : compoundCommand;
    }

    /**
     * Created the selected features set with the feature given its name and all its ancestors until the root.
     * @param featureModel The feature model to look into
     * @param featureName The name of feature to look for
     * @return The set of selected features including the feature and all its ancestors
     */
    private HashSet<COREFeature> getSelectedFeatures(COREFeatureModel featureModel, String featureName) {
        HashSet<COREFeature> selectedFeatures = new HashSet<COREFeature>();
        COREFeature feature = getFeatureByName(featureModel.getFeatures(), featureName);
        
        if (feature != null) {
            selectedFeatures.add(feature);
            while (feature.getParent() != null) {
                feature = feature.getParent();
                selectedFeatures.add(feature);
            }
        }
        
        return selectedFeatures;
    }


    /**
     * Finds a {@link COREFeature} in a featureModel from the given name.
     * @param features The list of features to look into.
     * @param featureName The name of the feature to look for.
     * @return the core feature
     */
    private COREFeature getFeatureByName(EList<COREFeature> features, String featureName) {
        for (COREFeature feature : features) {
            if (featureName.equals(feature.getName())) {
                return feature;
            }
        }
        return null;
    }

    /**
     * Deletes the given {@link Association}.
     *
     * @param aspect the current {@link Aspect}
     * @param association the {@link Association} to delete
     */
    public void deleteAssociation(Aspect aspect, Association association) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(association);
        CompoundCommand compoundCommand = new CompoundCommand();

        compoundCommand.append(deleteAssociation(aspect, association, editingDomain));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Deletes the given {@link Association}.
     *
     * @param aspect the current {@link Aspect}
     * @param association the {@link Association} to delete
     * @param editingDomain the {@link EditingDomain} to use for executing commands
     *
     * @return the created {@link CompoundCommand}
     */
    public CompoundCommand deleteAssociation(Aspect aspect, Association association, EditingDomain editingDomain) {
        CompoundCommand compoundCommand = new CompoundCommand();

        // create remove command for association
        compoundCommand.append(RemoveCommand.create(editingDomain, association));

        // create remove commands for association ends
        for (AssociationEnd associationEnd : association.getEnds()) {
            if (associationEnd.getFeatureSelection() != null) {
                compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, associationEnd));
            }
            compoundCommand.append(RemoveCommand.create(editingDomain, associationEnd));
        }

        return compoundCommand;
    }

    /**
     * Sets the key mapping and updates all operations with the proper mapping.
     * @param keyMapping The mapping for which to set the key
     * @param value The value of the key
     * @param dataClassifier The data classifier to update its references to |Key
     */
    public void setKeySelection(ClassifierMapping keyMapping, Object value, Classifier dataClassifier) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(keyMapping);
        CompoundCommand compoundCommand = new CompoundCommand();
        Classifier keyClassifier = keyMapping.getFrom();
        Classifier newKeyType = (Classifier) value;
        COREModelComposition modelComposition = (COREModelComposition) keyMapping.eContainer();
        ClassifierMapping dataClassifierMapping = null;

        for (COREModelElementComposition<?> composition : modelComposition.getCompositions()) {
            COREMapping<?> mapping = (COREMapping<?>) composition;
                        
            if (dataClassifier.equals(mapping.getTo())) {
                dataClassifierMapping = (ClassifierMapping)  mapping;
            }
        }

        compoundCommand.append(SetCommand.create(editingDomain, keyMapping,
                CorePackage.Literals.CORE_LINK__TO, value));

        for (OperationMapping operationMapping : dataClassifierMapping.getOperationMappings()) {
            Operation operationFrom = operationMapping.getFrom();
            Operation operationTo = operationMapping.getTo();

            for (int i = 0; i < operationFrom.getParameters().size(); i++) {
                if (keyClassifier.equals(operationFrom.getParameters().get(i).getType())) {
                    compoundCommand.append(SetCommand.create(editingDomain, operationTo.getParameters().get(i),
                            RamPackage.Literals.PARAMETER__TYPE, newKeyType));
                }
            }

            if (keyClassifier.equals(operationFrom.getReturnType())) {
                compoundCommand.append(SetCommand.create(editingDomain, operationTo,
                      RamPackage.Literals.OPERATION__RETURN_TYPE, newKeyType));
            }
        }

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the multiplicity of the given {@link AssociationEnd} according to the given lower and upper bound.
     *
     * @param aspect the current aspect
     * @param associationEnd the {@link AssociationEnd} the multiplicity should be changed for
     * @param lowerBound the lower bound of the association end
     * @param upperBound the upper bound of the association end, -1 for many
     */
    public void setMultiplicity(Aspect aspect, AssociationEnd associationEnd, int lowerBound, int upperBound) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        AssociationEnd oppositeEnd = associationEnd.getOppositeEnd();
        CompoundCommand compoundCommand;
        if (oppositeEnd.isNavigable()) {
            compoundCommand = new BidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd);
        } else {
            compoundCommand = new UnidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd);
            compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, associationEnd));
        }
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                RamPackage.Literals.PROPERTY__LOWER_BOUND, lowerBound));
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                RamPackage.Literals.PROPERTY__UPPER_BOUND, upperBound));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the role name of the given {@link AssociationEnd}.
     *
     * @param aspect the current aspect
     * @param associationEnd the {@link AssociationEnd} the role name should be changed for
     * @param roleName The new role name of the association end
     */
    public void setRoleName(Aspect aspect, AssociationEnd associationEnd, String roleName) {
        if (roleName.equals(associationEnd.getName())) {
            // do nothing if role name is unchanged
            return;
        }
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        if (associationEnd.getFeatureSelection() != null) {
            COREModelReuse modelReuse = associationEnd.getFeatureSelection();
            for (COREModelElementComposition<?> composition : modelReuse.getCompositions()) {
                COREMapping<?> mapping = (COREMapping<?>) composition;
                
                if (associationEnd.getClassifier().equals(mapping.getTo())) {
                    ClassifierMapping classifierMapping = (ClassifierMapping) mapping;

                    for (OperationMapping operationMapping : classifierMapping.getOperationMappings()) {
                        compoundCommand.append(SetCommand.create(editingDomain, operationMapping.getTo(),
                                CorePackage.Literals.CORE_NAMED_ELEMENT__NAME,
                                getOperationName(operationMapping.getFrom().getName(), roleName,
                                        operationMapping.getFrom().getParameters())));
                    }
                }
            }
        }
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                CorePackage.Literals.CORE_NAMED_ELEMENT__NAME, roleName));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Switches the navigable property of the given association end.
     * In case the opposite association end isn't navigable, both of the ends are switched
     *
     * @param aspect the current {@link Aspect}
     * @param associationEnd the {@link AssociationEnd} the navigable property should be switched of
     * @param oppositeEnd the opposite {@link AssociationEnd} from which the navigable property should be switched of
     */
    public void switchNavigable(Aspect aspect, AssociationEnd associationEnd, AssociationEnd oppositeEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand;

        if (associationEnd.isNavigable()) {
            if (oppositeEnd.isNavigable()) {
                compoundCommand = new UnidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd);
                compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, oppositeEnd));
                compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, associationEnd));
            } else {
                compoundCommand = new BidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd);
            }
        } else {
            // Switch navigability
            compoundCommand = new UnidirectionalCompoundCommand(aspect, associationEnd, oppositeEnd);
            compoundCommand.append(deleteFeatureSelection(compoundCommand, aspect, oppositeEnd));
            compoundCommand.append(SetCommand.create(editingDomain, associationEnd,
                    RamPackage.Literals.ASSOCIATION_END__NAVIGABLE, !associationEnd.isNavigable()));
        }

        compoundCommand.append(SetCommand.create(editingDomain, oppositeEnd,
                RamPackage.Literals.ASSOCIATION_END__NAVIGABLE, !oppositeEnd.isNavigable()));

        doExecute(editingDomain, compoundCommand);
    }

    /**
     * Sets the reference type of the given association end to the new reference type.
     *
     * @param associationEnd the {@link AssociationEnd} the reference type should be changed of
     * @param referenceType the new {@link ReferenceType} to set
     */
    public void setReferenceType(AssociationEnd associationEnd, ReferenceType referenceType) {
        doSet(associationEnd, RamPackage.Literals.PROPERTY__REFERENCE_TYPE, referenceType);
    }

    /**
     * Switches the static property of the given association end.
     *
     * @param associationEnd the {@link AssociationEnd} the static property should be switched of
     */
    public void switchStatic(AssociationEnd associationEnd) {
        EditingDomain editingDomain = EMFEditUtil.getEditingDomain(associationEnd);
        CompoundCommand compoundCommand = new CompoundCommand();
        
        // Check if we should set the relevant methods to static 
        if (associationEnd.getFeatureSelection() != null) {
            List<COREModelElementComposition<?>> compositions =
                    associationEnd.getFeatureSelection().getCompositions();
            for (COREModelElementComposition<?> composition : compositions) {
                COREMapping<?> mapping = (COREMapping<?>) composition;

                if (associationEnd.getClassifier().equals(mapping.getTo())) {
                    ClassifierMapping classifierMapping = (ClassifierMapping) mapping;
                    for (OperationMapping operationMapping : classifierMapping.getOperationMappings()) {
                        Operation op = operationMapping.getTo();
                        compoundCommand.append(SetCommand.create(editingDomain, op, 
                                RamPackage.Literals.OPERATION__STATIC, !op.isStatic()));
                    }
                }
            }
        }
        compoundCommand.append(SetCommand.create(editingDomain, associationEnd, 
                RamPackage.Literals.STRUCTURAL_FEATURE__STATIC, !associationEnd.isStatic()));
        doExecute(editingDomain, compoundCommand);
    }

    /**
     * A specialized compound command for creating and deleting association feature selection.
     * @author celinebensoussan
     *
     */
    public class AssociationCompoundCommand extends CompoundCommand {

        /**
         * The current aspect.
         */
        protected Aspect aspect;

        /**
         * The association end.
         */
        protected AssociationEnd associationEnd;

        /**
         * The opposite end.
         */
        protected AssociationEnd oppositeEnd;

        /**
         * The selected features.
         * It will contain values if a use has made a selection, otherwise it will always be set empty.
         */
        protected COREReuseConfiguration reuseConfiguration;

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public AssociationCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            this.aspect = aspect;
            this.associationEnd = associationEnd;
            this.oppositeEnd = oppositeEnd;
            if (associationEnd.getFeatureSelection() != null && associationEnd.getUpperBound() > 1) {
             // Get the configuration with the user selection which is different from the selected configuration
                this.reuseConfiguration = COREConfigurationUtil.getConfigurationByName(
                        associationEnd.getFeatureSelection().getReuse(), Constants.USER_SELECTED_CONFIGURATION_NAME);
            } else {
                this.reuseConfiguration = null;
            }
        }

        /**
         * Constructor with selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The selected features
         */
        public AssociationCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREReuseConfiguration reuseConfiguration) {
            this.aspect = aspect;
            this.associationEnd = associationEnd;
            this.oppositeEnd = oppositeEnd;
            this.reuseConfiguration = reuseConfiguration;
        }

    }

    /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a unidirectional association.
     * @author celinebensoussan
     *
     */
    public class UnidirectionalCompoundCommand extends AssociationCompoundCommand {

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public UnidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(aspect, associationEnd, oppositeEnd);
        }

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The configuration manually selected by the user
         */
        public UnidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREReuseConfiguration reuseConfiguration) {
            super(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        }

        @Override
        public void execute() {
            super.execute();
            appendAndExecute(createFeatureSelection(aspect, associationEnd, oppositeEnd, reuseConfiguration, null));
        }
    }

    /**
     * A compound command that extends the association compound command.
     * It is used to set the feature selection of a bidirectional association.
     * @author celinebensoussan
     *
     */
    public class BidirectionalCompoundCommand extends AssociationCompoundCommand {

        private COREReuseConfiguration oppositeSelectedConfiguration;

        /**
         * Constructor without selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         */
        public BidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd) {
            super(aspect, associationEnd, oppositeEnd);
        }

        /**
         * Constructor with selected features.
         * @param aspect The current aspect
         * @param associationEnd The association end
         * @param oppositeEnd The opposite end
         * @param reuseConfiguration The configuration manually selected by the user
         */
        public BidirectionalCompoundCommand(Aspect aspect, AssociationEnd associationEnd,
                AssociationEnd oppositeEnd, COREReuseConfiguration reuseConfiguration) {
            super(aspect, associationEnd, oppositeEnd, reuseConfiguration);
        }

        @Override
        public void execute() {
            super.execute();
            appendAndExecute(deleteFeatureSelection(this, aspect, associationEnd));
            if (oppositeEnd.getFeatureSelection() != null) {
                COREReuseConfiguration oppositeConfiguration = COREConfigurationUtil.getConfigurationByName(
                        oppositeEnd.getFeatureSelection().getReuse(), Constants.USER_SELECTED_CONFIGURATION_NAME);
                
                // Copy the user selection to ensure it is not contained in another reuse.
                // Otherwise, using it later will cause strange side effects like the user selection disappearing.
                // See issue #472.
                if (oppositeConfiguration != null) {
                    oppositeSelectedConfiguration =
                            COREConfigurationUtil.getConfigurationCopy(oppositeConfiguration, false, false);
                }
                appendAndExecute(deleteFeatureSelection(this, aspect, oppositeEnd));
            }
            appendAndExecute(createFeatureSelection(aspect, associationEnd, oppositeEnd,
                    reuseConfiguration, oppositeSelectedConfiguration));
            appendAndExecute(createFeatureSelection(aspect, oppositeEnd, associationEnd,
                    oppositeSelectedConfiguration, reuseConfiguration));
        }
    }
}