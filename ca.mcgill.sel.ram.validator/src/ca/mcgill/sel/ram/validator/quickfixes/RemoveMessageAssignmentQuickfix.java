package ca.mcgill.sel.ram.validator.quickfixes;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.controller.ControllerFactory;

/**
 * Sets the "assignTo" value of a message to <code>null</code>.
 * 
 * @author emmanuelngch
 */
public class RemoveMessageAssignmentQuickfix implements Quickfix {

    private Message target;

    /**
     * Builds a {@link RemoveVoidReturnValueQuickfix}.
     * 
     * @param target the target {@link EObject}
     */
    public RemoveMessageAssignmentQuickfix(EObject target) {
        this.target = (Message) target;
    }

    @Override
    public String getLabel() {
        return "Remove assignment";
    }

    @Override
    public void quickfix() {
        ControllerFactory.INSTANCE.getMessageController().changeAssignTo(target, null);
    }

}
