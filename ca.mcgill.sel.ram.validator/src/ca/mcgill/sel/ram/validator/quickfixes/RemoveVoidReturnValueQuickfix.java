package ca.mcgill.sel.ram.validator.quickfixes;

import org.eclipse.emf.ecore.EObject;

import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.controller.ControllerFactory;

/**
 * Sets the return values in void methods to <code>null</code>.
 * 
 * @author emmanuelngch
 */
public class RemoveVoidReturnValueQuickfix implements Quickfix {

    private Message target;

    /**
     * Builds the {@link RemoveVoidReturnValueQuickfix}.
     * 
     * @param target the target {@link EObject}
     */
    public RemoveVoidReturnValueQuickfix(EObject target) {
        this.target = (Message) target;
    }

    @Override
    public String getLabel() {
        return "Remove the return value";
    }

    @Override
    public void quickfix() {
        ControllerFactory.INSTANCE.getMessageController().changeReturnValue(target, null);
    }

}
