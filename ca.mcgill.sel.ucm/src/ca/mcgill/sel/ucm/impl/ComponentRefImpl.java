/**
 */
package ca.mcgill.sel.ucm.impl;

import ca.mcgill.sel.ucm.Component;
import ca.mcgill.sel.ucm.ComponentRef;
import ca.mcgill.sel.ucm.PathNode;
import ca.mcgill.sel.ucm.UCMPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component Ref</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ca.mcgill.sel.ucm.impl.ComponentRefImpl#getCompDef <em>Comp Def</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.ComponentRefImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.ComponentRefImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link ca.mcgill.sel.ucm.impl.ComponentRefImpl#getNodes <em>Nodes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ComponentRefImpl extends UCMModelElementImpl implements ComponentRef {
    /**
     * The cached value of the '{@link #getCompDef() <em>Comp Def</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getCompDef()
     * @generated
     * @ordered
     */
    protected Component compDef;

    /**
     * The cached value of the '{@link #getChildren() <em>Children</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getChildren()
     * @generated
     * @ordered
     */
    protected EList<ComponentRef> children;

    /**
     * The cached value of the '{@link #getParent() <em>Parent</em>}' reference.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getParent()
     * @generated
     * @ordered
     */
    protected ComponentRef parent;

    /**
     * The cached value of the '{@link #getNodes() <em>Nodes</em>}' reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getNodes()
     * @generated
     * @ordered
     */
    protected EList<PathNode> nodes;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected ComponentRefImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return UCMPackage.Literals.COMPONENT_REF;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Component getCompDef() {
        if (compDef != null && compDef.eIsProxy()) {
            InternalEObject oldCompDef = (InternalEObject)compDef;
            compDef = (Component)eResolveProxy(oldCompDef);
            if (compDef != oldCompDef) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UCMPackage.COMPONENT_REF__COMP_DEF, oldCompDef, compDef));
            }
        }
        return compDef;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public Component basicGetCompDef() {
        return compDef;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetCompDef(Component newCompDef, NotificationChain msgs) {
        Component oldCompDef = compDef;
        compDef = newCompDef;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UCMPackage.COMPONENT_REF__COMP_DEF, oldCompDef, newCompDef);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setCompDef(Component newCompDef) {
        if (newCompDef != compDef) {
            NotificationChain msgs = null;
            if (compDef != null)
                msgs = ((InternalEObject)compDef).eInverseRemove(this, UCMPackage.COMPONENT__COMP_REFS, Component.class, msgs);
            if (newCompDef != null)
                msgs = ((InternalEObject)newCompDef).eInverseAdd(this, UCMPackage.COMPONENT__COMP_REFS, Component.class, msgs);
            msgs = basicSetCompDef(newCompDef, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UCMPackage.COMPONENT_REF__COMP_DEF, newCompDef, newCompDef));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<ComponentRef> getChildren() {
        if (children == null) {
            children = new EObjectWithInverseResolvingEList<ComponentRef>(ComponentRef.class, this, UCMPackage.COMPONENT_REF__CHILDREN, UCMPackage.COMPONENT_REF__PARENT);
        }
        return children;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ComponentRef getParent() {
        if (parent != null && parent.eIsProxy()) {
            InternalEObject oldParent = (InternalEObject)parent;
            parent = (ComponentRef)eResolveProxy(oldParent);
            if (parent != oldParent) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, UCMPackage.COMPONENT_REF__PARENT, oldParent, parent));
            }
        }
        return parent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public ComponentRef basicGetParent() {
        return parent;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public NotificationChain basicSetParent(ComponentRef newParent, NotificationChain msgs) {
        ComponentRef oldParent = parent;
        parent = newParent;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UCMPackage.COMPONENT_REF__PARENT, oldParent, newParent);
            if (msgs == null) msgs = notification; else msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public void setParent(ComponentRef newParent) {
        if (newParent != parent) {
            NotificationChain msgs = null;
            if (parent != null)
                msgs = ((InternalEObject)parent).eInverseRemove(this, UCMPackage.COMPONENT_REF__CHILDREN, ComponentRef.class, msgs);
            if (newParent != null)
                msgs = ((InternalEObject)newParent).eInverseAdd(this, UCMPackage.COMPONENT_REF__CHILDREN, ComponentRef.class, msgs);
            msgs = basicSetParent(newParent, msgs);
            if (msgs != null) msgs.dispatch();
        }
        else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, UCMPackage.COMPONENT_REF__PARENT, newParent, newParent));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<PathNode> getNodes() {
        if (nodes == null) {
            nodes = new EObjectWithInverseResolvingEList<PathNode>(PathNode.class, this, UCMPackage.COMPONENT_REF__NODES, UCMPackage.PATH_NODE__COMP_REF);
        }
        return nodes;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.COMPONENT_REF__COMP_DEF:
                if (compDef != null)
                    msgs = ((InternalEObject)compDef).eInverseRemove(this, UCMPackage.COMPONENT__COMP_REFS, Component.class, msgs);
                return basicSetCompDef((Component)otherEnd, msgs);
            case UCMPackage.COMPONENT_REF__CHILDREN:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getChildren()).basicAdd(otherEnd, msgs);
            case UCMPackage.COMPONENT_REF__PARENT:
                if (parent != null)
                    msgs = ((InternalEObject)parent).eInverseRemove(this, UCMPackage.COMPONENT_REF__CHILDREN, ComponentRef.class, msgs);
                return basicSetParent((ComponentRef)otherEnd, msgs);
            case UCMPackage.COMPONENT_REF__NODES:
                return ((InternalEList<InternalEObject>)(InternalEList<?>)getNodes()).basicAdd(otherEnd, msgs);
        }
        return super.eInverseAdd(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case UCMPackage.COMPONENT_REF__COMP_DEF:
                return basicSetCompDef(null, msgs);
            case UCMPackage.COMPONENT_REF__CHILDREN:
                return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
            case UCMPackage.COMPONENT_REF__PARENT:
                return basicSetParent(null, msgs);
            case UCMPackage.COMPONENT_REF__NODES:
                return ((InternalEList<?>)getNodes()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case UCMPackage.COMPONENT_REF__COMP_DEF:
                if (resolve) return getCompDef();
                return basicGetCompDef();
            case UCMPackage.COMPONENT_REF__CHILDREN:
                return getChildren();
            case UCMPackage.COMPONENT_REF__PARENT:
                if (resolve) return getParent();
                return basicGetParent();
            case UCMPackage.COMPONENT_REF__NODES:
                return getNodes();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case UCMPackage.COMPONENT_REF__COMP_DEF:
                setCompDef((Component)newValue);
                return;
            case UCMPackage.COMPONENT_REF__CHILDREN:
                getChildren().clear();
                getChildren().addAll((Collection<? extends ComponentRef>)newValue);
                return;
            case UCMPackage.COMPONENT_REF__PARENT:
                setParent((ComponentRef)newValue);
                return;
            case UCMPackage.COMPONENT_REF__NODES:
                getNodes().clear();
                getNodes().addAll((Collection<? extends PathNode>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case UCMPackage.COMPONENT_REF__COMP_DEF:
                setCompDef((Component)null);
                return;
            case UCMPackage.COMPONENT_REF__CHILDREN:
                getChildren().clear();
                return;
            case UCMPackage.COMPONENT_REF__PARENT:
                setParent((ComponentRef)null);
                return;
            case UCMPackage.COMPONENT_REF__NODES:
                getNodes().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case UCMPackage.COMPONENT_REF__COMP_DEF:
                return compDef != null;
            case UCMPackage.COMPONENT_REF__CHILDREN:
                return children != null && !children.isEmpty();
            case UCMPackage.COMPONENT_REF__PARENT:
                return parent != null;
            case UCMPackage.COMPONENT_REF__NODES:
                return nodes != null && !nodes.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //ComponentRefImpl
