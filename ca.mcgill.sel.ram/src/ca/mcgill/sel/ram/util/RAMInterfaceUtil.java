package ca.mcgill.sel.ram.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREVisibilityType;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Classifier;
import ca.mcgill.sel.ram.ObjectType;
import ca.mcgill.sel.ram.PrimitiveType;
import ca.mcgill.sel.ram.RArray;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.REnumLiteral;
import ca.mcgill.sel.ram.RamPackage;

/**
 * Utility class with helper methods to retrieve available properties of a given model.
 * This reflects the available properties as per usage and customization defined by CORE.
 * 
 * @author mschoettle
 */
public final class RAMInterfaceUtil {

    /**
     * Creates a new instance.
     */
    private RAMInterfaceUtil() {

    }

    /**
     * Returns all primitive types of the model the object is contained in.
     * This includes enums and arrays of primitive types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of primitive types of the model
     */
    public static Collection<PrimitiveType> getAvailablePrimitiveTypes(EObject currentObject) {
        Collection<PrimitiveType> result = new ArrayList<>();
        Aspect aspect = getAspect(currentObject);

        // Collect all primitive types, except arrays of non-primitive types.
        Collection<PrimitiveType> primitiveTypes =
                EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(), RamPackage.Literals.PRIMITIVE_TYPE);

        for (PrimitiveType type : primitiveTypes) {
            // Ignore arrays with a non-primitive type.
            // Add enums through respective method below to include those from extended models.
            if (type instanceof RArray
                    && !(((RArray) type).getType() instanceof PrimitiveType)
                    || type instanceof REnum) {
                continue;
            }

            result.add(type);
        }

        result.addAll(getAvailableEnums(aspect));

        return result;
    }

    /**
     * Returns all enums of the model the object is contained in as well as extended models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of enums
     */
    public static Collection<REnum> getAvailableEnums(EObject currentObject) {
        Collection<REnum> result = new ArrayList<>();

        Set<Aspect> extendedAspects = getExtendedAspects(currentObject);

        for (Aspect aspect : extendedAspects) {
            Collection<REnum> enums =
                    EcoreUtil.getObjectsByType(aspect.getStructuralView().getTypes(), RamPackage.Literals.RENUM);
            result.addAll(enums);
        }

        return result;
    }

    /**
     * Returns all enums of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of enums
     */
    public static Collection<REnum> getAvailableExternalEnums(EObject currentObject) {
        Aspect currentAspect = (Aspect) EcoreUtil.getRootContainer(currentObject);

        Collection<REnum> result = new HashSet<>();
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(currentAspect);
        
        for (COREModelComposition composition : allCompositions) {
            Aspect sourceAspect = (Aspect) composition.getSource();
            
            Collection<REnum> enums =
                    EcoreUtil.getObjectsByType(sourceAspect.getStructuralView().getTypes(), RamPackage.Literals.RENUM);
            
            for (REnum rEnum : enums) {
                if (CorePackage.Literals.CORE_MODEL_REUSE.isInstance(composition) 
                        && rEnum.getVisibility() != COREVisibilityType.PUBLIC) {
                    continue;
                }
                
                result.add(rEnum);
            }   
        }
        
        COREModelUtil.filterMappedElements(result, allCompositions);

        return result;
    }
    
    /**
     * Returns all classes of the model the object is contained in as well as extended models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableClasses(EObject currentObject) {
        Collection<Classifier> result = getAvailableExternalClasses(currentObject);

        //add the classes of the current model
        result.addAll(getAspect(currentObject).getStructuralView().getClasses());
        return result;
    }
        
    /**
     * Returns all classes of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableExternalClasses(EObject currentObject) {
        return getAvailableExternalClassifiers(currentObject, false);
    }

    /**
     * Returns all datatypes of the model the object is contained in as well as extended models that 
     * are identified as data types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableDataTypes(EObject currentObject) {
        Collection<Classifier> result = new HashSet<>();

        Aspect currentAspect = (Aspect) EcoreUtil.getRootContainer(currentObject);

        Set<Aspect> extendedAspects = getExtendedAspects(currentObject);

        for (Aspect aspect : extendedAspects) {
            for (Classifier classifier : aspect.getStructuralView().getClasses()) {
                Classifier resolvedClassifier = RAMModelUtil.resolveClassifier(currentAspect, classifier);
                if (resolvedClassifier.isDataType()) {
                    result.add(resolvedClassifier);
                }
            }
        }

        return result;
    }
    
    /**
     * Returns all data types of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of classes
     */
    public static Collection<Classifier> getAvailableExternalDatatypes(EObject currentObject) {
        return getAvailableExternalClassifiers(currentObject, true);
    }

    /**
     * Returns all available classes or datatypes of extended and reused models.
     * 
     * @param currentObject the object contained in a model
     * @param dataType <code>true</code> for data types, <code>false</code> to get all classes
     * @return the collection of classes
     */
    private static Collection<Classifier> getAvailableExternalClassifiers(EObject currentObject, boolean dataType) {
        Aspect currentAspect = (Aspect) EcoreUtil.getRootContainer(currentObject);

        Collection<Classifier> result = new HashSet<>();
        Set<COREModelComposition> allCompositions = COREModelUtil.collectAllModelCompositions(currentAspect);
        
        for (COREModelComposition modelComposition : allCompositions) {
            Aspect externalAspect = (Aspect) modelComposition.getSource();
            
            for (Classifier classifier : externalAspect.getStructuralView().getClasses()) {
                if (CorePackage.Literals.CORE_MODEL_REUSE.isInstance(modelComposition) 
                        && classifier.getVisibility() != COREVisibilityType.PUBLIC) {
                    continue;
                }
                
                if (dataType == classifier.isDataType()) {
                    result.add(classifier);
                }
            }   
        }
        
        COREModelUtil.filterMappedElements(result, allCompositions);

        return result;
    }

    /**
     * Returns all types that are available to be used as a type for attributes.
     * This includes data types, enums and primitive types.
     * 
     * @param currentObject the object contained in a model
     * @return the collection of types
     */
    public static Collection<ObjectType> getAvailableAttributeTypes(EObject currentObject) {
        Collection<ObjectType> result = new HashSet<>();

        result.addAll(getAvailableDataTypes(currentObject));
        result.addAll(getAvailablePrimitiveTypes(currentObject));

        return result;
    }
    
    /**
     * Returns all enum literals of the available enums.
     * If the given object is an enum, only the enum literals of the given enum are returned.
     * 
     * @param currentObject the object contained in a model, an {@link REnum} if only its literals should be retrieved
     * @return the collection of enum literals
     */
    public static Collection<REnumLiteral> getAvailableLiterals(EObject currentObject) {
        Collection<REnumLiteral> literals = new HashSet<>();
        
        Collection<REnum> enums = getAvailableEnums(currentObject);
        
        if (currentObject instanceof REnum) {
            REnum specificEnum = (REnum) currentObject;
            literals.addAll(specificEnum.getLiterals());
        } else {
            for (REnum currentEnum : enums) {
                literals.addAll(currentEnum.getLiterals());
            }            
        }
        
        return literals;
    }

    /**
     * Returns the aspect the given object is contained in.
     * If the object is the aspect itself, the object is returned.
     * 
     * @param currentObject the object contained in a model
     * @return the aspect the given object is contained in, null if none found
     */
    private static Aspect getAspect(EObject currentObject) {
        Aspect aspect = EMFModelUtil.getRootContainerOfType(currentObject, RamPackage.Literals.ASPECT);

        if (aspect == null && currentObject instanceof Aspect) {
            aspect = (Aspect) currentObject;
        }

        return aspect;
    }

    /**
     * Returns the set of extended aspects within whose hierarchy the given object is contain in.
     * The set contains the aspect the given object is contained in.
     * 
     * @param currentObject the object contained in a model
     * @return the set of extended aspects, including the aspect of the object
     */
    private static Set<Aspect> getExtendedAspects(EObject currentObject) {
        Aspect aspect = getAspect(currentObject);

        // Create a set of aspects where elements are allowed from.
        // Besides the current aspect, add extended aspects
        Set<Aspect> includedAspects = COREModelUtil.collectExtendedModels(aspect);
        includedAspects.add(aspect);

        return includedAspects;
    }

}
