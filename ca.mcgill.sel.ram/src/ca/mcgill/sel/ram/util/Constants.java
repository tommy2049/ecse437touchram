package ca.mcgill.sel.ram.util;

import org.eclipse.emf.common.util.URI;

import ca.mcgill.sel.commons.ResourceUtil;

/**
 * A utility class that provides constants related to the association concern.
 *
 * @author cbensoussan
 *
 */
public final class Constants {

    /**
     * The location of the association concern file.
     */
    public static final URI ASSOCIATION_CONCERN_LOCATION = URI.createFileURI(ResourceUtil.getResourcePath("models")
            + "/Association/Association.core");

    /**
     * The file extension for aspects.
     */
    public static final String ASPECT_FILE_EXTENSION = "ram";

    /**
     * The file extension for concerns.
     */
    public static final String CORE_FILE_EXTENSION = "core";

    /**
     * The file extension for jucm.
     */
    public static final String JUCM_FILE_EXTENSION = "jucm";

    /**
     * The file extension for jars.
     */
    public static final String JAR_FILE_EXTENSION = "jar";

    /**
     * String appended to woven concern when saved.
     */
    public static final String WOVEN_PREFIX = "Woven_";

    /**
     * String appended to concern when saving the reuse.
     */
    public static final String REUSED_PREFIX = "Reused_";

    /**
     * The association concern name.
     */
    public static final String ASSOCIATION_CONCERN_NAME = "Association";

    /**
     * The name of the minimum feature.
     */
    public static final String MINIMUM_FEATURE_NAME = "Minimum";

    /**
     * The name of the maximum feature.
     */
    public static final String MAXIMUM_FEATURE_NAME = "Maximum";

    /**
     * The name of the bidirectional.
     */
    public static final String BIDIRECTIONAL_FEATURE_NAME = "Bidirectional";

    /**
     * The name of the data class.
     */
    public static final String DATA_CLASS_NAME = "Data";

    /**
     * The name of the associated class.
     */
    public static final String ASSOCIATED_CLASS_NAME = "Associated";

    /**
     * The name of the key class.
     */
    public static final String KEY_CLASS_NAME = "Key";

    /**
     * The name of the value class.
     */
    public static final String VALUE_CLASS_NAME = "Value";

    /**
     * Name of association end corresponding to the collection.
     */
    public static final String ASSOCIATION_END_NAME_COLLECTION = "collection";

    /**
     * Name of association end corresponding to the collection.
     */
    public static final String ASSOCIATION_END_NAME_ELEMENTS = "elements";

    /**
     * Name of association end corresponding to the collection.
     */
    public static final String ASSOCIATION_END_NAME_ASSOCIATED = "myAssociated";

    /**
     * Association name between data and collection.
     */
    public static final String ASSOCIATION_NAME_DATA_COLLECTION = "Data_CollectionOfAssociated";

    /**
     * Name of the feature One.
     */
    public static final String FEATURE_NAME_ONE = "One";

    /**
     * Name of the feature Many.
     */
    public static final String FEATURE_NAME_MANY = "Many";

    /**
     * Name of the feature Many.
     */
    public static final String FEATURE_NAME_PLAIN = "Plain";

    /**
     * Name of the feature Many.
     */
    public static final String FEATURE_NAME_ORDERED = "Ordered";

    /**
     * Name of the feature Maximum.
     */
    public static final String FEATURE_NAME_MAXIMUM = "Maximum";

    /**
     * Name of the feature Minimum.
     */
    public static final String FEATURE_NAME_MINIMUM = "Minimum";

    /**
     * Name of the feature One Opposite.
     */
    public static final String FEATURE_NAME_ONE_OPPOSITE = "OneOpposite";

    /**
     * Name of the feature Many Opposite.
     */
    public static final String FEATURE_NAME_MANY_OPPOSITE = "ManyOpposite";

    /**
     * Name of the feature Plain Opposite.
     */
    public static final String FEATURE_NAME_PLAIN_OPPOSITE = "PlainOpposite";

    /**
     * Name of the feature Ordered Opposite.
     */
    public static final String FEATURE_NAME_ORDERED_OPPOSITE = "OrderedOpposite";

    /**
     * Name of the configuration created when selection is made by a user.
     */
    public static final String USER_SELECTED_CONFIGURATION_NAME = "User selection";

    /**
     * Creates a new instance.
     */
    private Constants() {
    }
}
