/**
 */
package ca.mcgill.sel.ram;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.Class#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getClass_()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='notSelfSuperType'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot notSelfSuperType='Tuple {\n\tmessage : String = \'A class may not be it\\\'s own supertype\',\n\tstatus : Boolean = not self.superTypes-&gt;includes(self)\n}.status'"
 * @generated
 */
public interface Class extends Classifier {
    /**
     * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ram.Attribute}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Attributes</em>' containment reference list.
     * @see ca.mcgill.sel.ram.RamPackage#getClass_Attributes()
     * @model containment="true"
     * @generated
     */
    EList<Attribute> getAttributes();

} // Class
