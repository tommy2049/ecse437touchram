/**
 */
package ca.mcgill.sel.ram;

import ca.mcgill.sel.core.COREMapping;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Enum Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.EnumMapping#getEnumLiteralMappings <em>Enum Literal Mappings</em>}</li>
 * </ul>
 *
 * @see ca.mcgill.sel.ram.RamPackage#getEnumMapping()
 * @model
 * @generated
 */
public interface EnumMapping extends COREMapping<REnum> {
    /**
     * Returns the value of the '<em><b>Enum Literal Mappings</b></em>' containment reference list.
     * The list contents are of type {@link ca.mcgill.sel.ram.EnumLiteralMapping}.
     * <!-- begin-user-doc -->
     * <p>
     * If the meaning of the '<em>Enum Literal Mappings</em>' containment reference list isn't clear,
     * there really should be more of a description here...
     * </p>
     * <!-- end-user-doc -->
     * @return the value of the '<em>Enum Literal Mappings</em>' containment reference list.
     * @see ca.mcgill.sel.ram.RamPackage#getEnumMapping_EnumLiteralMappings()
     * @model containment="true"
     * @generated
     */
    EList<EnumLiteralMapping> getEnumLiteralMappings();

} // EnumMapping
