/**
 */
package ca.mcgill.sel.ram.impl;

import ca.mcgill.sel.core.impl.COREMappingImpl;

import ca.mcgill.sel.ram.EnumLiteralMapping;
import ca.mcgill.sel.ram.EnumMapping;
import ca.mcgill.sel.ram.REnum;
import ca.mcgill.sel.ram.RamPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum Mapping</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.mcgill.sel.ram.impl.EnumMappingImpl#getEnumLiteralMappings <em>Enum Literal Mappings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumMappingImpl extends COREMappingImpl<REnum> implements EnumMapping {
    /**
     * The cached value of the '{@link #getEnumLiteralMappings() <em>Enum Literal Mappings</em>}' containment reference list.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see #getEnumLiteralMappings()
     * @generated
     * @ordered
     */
    protected EList<EnumLiteralMapping> enumLiteralMappings;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    protected EnumMappingImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return RamPackage.Literals.ENUM_MAPPING;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    public EList<EnumLiteralMapping> getEnumLiteralMappings() {
        if (enumLiteralMappings == null) {
            enumLiteralMappings = new EObjectContainmentEList<EnumLiteralMapping>(EnumLiteralMapping.class, this, RamPackage.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS);
        }
        return enumLiteralMappings;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
            case RamPackage.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS:
                return ((InternalEList<?>)getEnumLiteralMappings()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
            case RamPackage.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS:
                return getEnumLiteralMappings();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
            case RamPackage.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS:
                getEnumLiteralMappings().clear();
                getEnumLiteralMappings().addAll((Collection<? extends EnumLiteralMapping>)newValue);
                return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
            case RamPackage.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS:
                getEnumLiteralMappings().clear();
                return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
            case RamPackage.ENUM_MAPPING__ENUM_LITERAL_MAPPINGS:
                return enumLiteralMappings != null && !enumLiteralMappings.isEmpty();
        }
        return super.eIsSet(featureID);
    }

} //EnumMappingImpl
