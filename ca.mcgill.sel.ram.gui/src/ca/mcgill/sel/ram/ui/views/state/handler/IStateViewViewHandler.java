package ca.mcgill.sel.ram.ui.views.state.handler;

import ca.mcgill.sel.ram.ui.events.listeners.IDragListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapListener;
import ca.mcgill.sel.ram.ui.views.state.StateViewView;

/**
 * Interface for handlers for state view views.
 * @author gemini
 *
 */
public interface IStateViewViewHandler extends IDragListener, ITapAndHoldListener, ITapListener {
    
    /**
     * Operation that removes a state view view.
     * @param stateViewView the state view view
     */
    void removeStateView(StateViewView stateViewView);
    
    /**
     * Operation that adds a state view view.
     * @param stateViewView the state view view
     */
    void addStateMachine(StateViewView stateViewView);
    
}
