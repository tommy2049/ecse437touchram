package ca.mcgill.sel.ram.ui.views.impact.handler.impl;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.COREWeightedLink;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.evaluator.im.PropagationResult;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamListComponent;
import ca.mcgill.sel.ram.ui.components.listeners.RamListListener;
import ca.mcgill.sel.ram.ui.views.impact.ImpactSelectDiagramView;

/**
 * The handler for {@link COREWeightedLink} in select mode.
 *
 * @author Romain
 *
 */
public class WeightedLinkSelectHandler implements RamListListener<COREWeightedLink> {

    @Override
    public void elementSelected(RamListComponent<COREWeightedLink> list, COREWeightedLink weightedLink) {
        // This list is suppose to be contains in a FeatureImpactNodeView that is contains in a ImpactDiagramView
        // In this handler, we handle the Select mode of impact model. So we retrieve the ImpactSelectDiagramView
        final ImpactSelectDiagramView diagram = list.getParentOfType(ImpactSelectDiagramView.class);

        // if this diagram is null, it means that the RamListComponent is not contains in a FeatureImpactNodeView that
        // is contains in a ImpactDiagramView
        // Or that this handler is use with the edit mode and not select mode.
        if (diagram != null) {
            COREModelReuse modelReuse =
                    EMFModelUtil.getRootContainerOfType(weightedLink,
                            CorePackage.Literals.CORE_MODEL_REUSE);

            COREReuse reuse = modelReuse.getReuse();

            PropagationResult propagationResult = diagram.getGoalImpactMap();
            propagationResult =
                    propagationResult == null ? null : propagationResult.getEvaluationsForReuse(reuse);

            showImpactModelReusedConcern(weightedLink.getFrom(), propagationResult, reuse);
        }
    }

    @Override
    public void elementDoubleClicked(RamListComponent<COREWeightedLink> list, COREWeightedLink element) {
    }

    @Override
    public void elementHeld(RamListComponent<COREWeightedLink> list, final COREWeightedLink weightedLink) {
    }

    /**
     * Show the ImpactModel of a reused Concern. It will be center on the from node of weightedLink.
     *
     * @param coreImpactNode the {@link COREImpactNode} to show as root.
     * @param propagationResult the Map that contains the impact for each goal.
     * @param reuse the reuse that contains the concern to show
     */
    private static void showImpactModelReusedConcern(COREImpactNode coreImpactNode,
            final PropagationResult propagationResult, COREReuse reuse) {

        RamApp.getApplication().showImpactModelSelectMode(reuse.getReusedConcern(),
                coreImpactNode, propagationResult);
    }
}