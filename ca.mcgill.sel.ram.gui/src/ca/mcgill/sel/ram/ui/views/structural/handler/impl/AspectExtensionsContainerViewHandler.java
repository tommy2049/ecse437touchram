package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import java.io.File;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ReuseController;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.browser.AspectFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.AspectFileBrowserListener;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;

/**
 * Handles events for a {@link ca.mcgill.sel.ram.ui.views.structural.CompositionContainerView} which is showing the
 * list of compositions in an aspect.
 * 
 * @author eyildirim
 * @author oalam
 */
public class AspectExtensionsContainerViewHandler implements ICompositionContainerViewHandler {
    
    @Override
    public void deleteModelComposition(COREModelComposition modelComposition) {
        // Disallow deleting the COREModelComposition if split view is enabled.
        boolean splitModeEnabled =
                RamApp.getActiveAspectScene().getCurrentView() instanceof CompositionSplitEditingView;
        if (!splitModeEnabled) {
            ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
            controller.removeModelComposition(modelComposition);
        } else {
            RamApp.getActiveAspectScene().displayPopup(Strings.POPUP_CANT_DELETE_INST_EDIT);
        }
    }

    @Override
    public void loadBrowser(final Aspect mainAspect) {

        // Ask the user to load an aspect
        AspectFileBrowser.loadAspect(new AspectFileBrowserListener() {

            @Override
            public void aspectLoaded(final Aspect aspect) {
                extendAspect(mainAspect, aspect);
            }

            @Override
            public void aspectSaved(File file) {
            }
        });
    }

    /**
     * Create an extend relationship between the given aspect.
     * Does nothing if the creation is not valid.
     *
     * @param extendingAspect - The aspect that extends
     * @param aspect - The aspect to extend.
     */
    private static void extendAspect(final Aspect extendingAspect, final Aspect aspect) {
        RamApp.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                String errorMessage = null;
                // Look for error cases.
                if (aspect.equals(extendingAspect)) {
                    errorMessage = Strings.POPUP_ERROR_SELF_EXTENDS;
                } else if (COREModelUtil.collectExtendedModels(aspect).contains(extendingAspect)) {
                    errorMessage = Strings.POPUP_ERROR_CYCLIC_EXTENDS;
                }
                // Check if this aspect is already extended. Cannot use collectExtendedAspects
                // because we don't want the check to be recursive here.
                for (COREModelComposition composition : extendingAspect.getModelExtensions()) {
                    if (composition.getSource() == aspect) {
                        errorMessage = Strings.POPUP_ERROR_SAME_EXTENDS;
                        break;
                    }
                }

                if (errorMessage != null) {
                    RamApp.getActiveScene().displayPopup(errorMessage, PopupType.ERROR);
                    return;
                }

                // We can create the composition.
                ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
                controller.createModelExtension(extendingAspect, aspect);
            }
        });
    }

}
