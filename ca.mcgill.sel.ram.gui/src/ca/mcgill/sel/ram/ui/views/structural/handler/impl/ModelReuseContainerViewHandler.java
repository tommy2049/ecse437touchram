package ca.mcgill.sel.ram.ui.views.structural.handler.impl;

import org.mt4j.sceneManagement.transition.BlendTransition;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.controller.ReuseController;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.browser.CoreFileBrowser;
import ca.mcgill.sel.ram.ui.components.browser.interfaces.CoreFileBrowserListener;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;
import ca.mcgill.sel.ram.ui.views.structural.CompositionSplitEditingView;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;

/**
 * Handles events for a {@link ca.mcgill.sel.ram.ui.views.structural.CompositionContainerView} which is showing the
 * list of model reuses in a model.
 *
 * @author oalam
 */
public class ModelReuseContainerViewHandler implements ICompositionContainerViewHandler {
    
    @Override
    public void deleteModelComposition(COREModelComposition modelComposition) {
        // Disallow deleting if split view is enabled.
        boolean splitModeEnabled =
                RamApp.getActiveAspectScene().getCurrentView() instanceof CompositionSplitEditingView;
        if (!splitModeEnabled) {
            ReuseController controller = COREControllerFactory.INSTANCE.getReuseController();
            controller.removeModelComposition(modelComposition);
        } else {
            RamApp.getActiveAspectScene().displayPopup("You can not delete the model reuse while editing it.");
        }
    }

    @Override
    public void loadBrowser(final Aspect aspect) {
        CoreFileBrowser.setInitialFolder(GUIConstants.DIRECTORY_LIBRARIES);

        // Ask the user to select an aspect that should be depended on
        CoreFileBrowser.loadCoreFile(new CoreFileBrowserListener() {

            @Override
            public void concernLoaded(COREConcern concern) {
                IConcernSelectSceneHandler handler = HandlerFactory.INSTANCE.getDisplayFMSceneSelectHandler();
                if (concern == null) {
                    RamApp.getActiveScene().displayPopup(Strings.POPUP_ERROR_SELF_REUSE);
                } else {
                    RamApp.getActiveScene().setTransition(new BlendTransition(RamApp.getApplication(), 700));
                    RamApp.getApplication().displayFMSelect(concern, aspect, handler);
                }
            }
        }, true);
    }

}
