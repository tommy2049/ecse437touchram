package ca.mcgill.sel.ram.ui.views.structural;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * An {@link CompositionTitleView} is a {@link RamRectangleComponent} that shows {@link COREModelComposition} name
 * along with buttons to do different operations.
 * It is used to give a general overview and as a shortcut to show the full details of its compositions.
 * 
 * @author eyildirim
 */
public class CompositionTitleView extends RamRectangleComponent implements ActionListener {

    private static final int ICON_SIZE = Fonts.FONTSIZE_COMPOSITION + 2;

    private static final String ACTION_COMPOSITION_SHOW_EXTERNAL_ASPECT = "view.composition.showExternalAspect";
    private static final String ACTION_COMPOSITION_DELETE = "view.composition.deleteComposition";
    private static final String ACTION_COMPOSITION_SHOW_MAPPING_DETAILS = "view.composition.showMappingDetails";
    private static final String ACTION_COMPOSITION_HIDE_MAPPING_DETAILS = "view.composition.hideMappingDetails";
    private static final String ACTION_COMPOSITION_TOGGLE_SPLIT_VIEW = "view.composition.switchToSplitView";
    private static final String ACTION_CLASSIFIER_MAPPING_ADD = "mapping.classifier.add";
    private static final String ACTION_ENUM_MAPPING_ADD = "mapping.enum.add";

    private CompositionView myCompositionView;
    private COREModelComposition modelComposition;

    /**
     * Button container for expanding or collapsing button.
     * These expand and collapse buttons will be used to show/hide the details of a composition.
     */
    private RamRectangleComponent expandCollapseButtonsContainer;

    /**
     * Button to show details of the composition.
     */
    private RamButton buttonExpand;

    /**
     * Button to hide details of the composition.
     */
    private RamButton buttonCollapse;

    /**
     * Button to view the full Aspect view.
     */
    private RamButton mappingButton;

    /**
     * Button to delete composition.
     */
    private RamButton deleteButton;

    /**
     * Button to add Classifier Mapping.
     */
    private RamButton buttonAddClassifierMapping;

    /**
     * Button to add Enum Mapping.
     */
    private RamButton buttonAddEnumMapping;

    /**
     * Name displayed as title. If the composition if for extending an aspect, it's the name of the aspect.
     * If it comes from a reuse, it's the name of the reuse.
     */
    private TextView modelReuseName;

    /**
     * Creates a new title view.
     * 
     * @param compositionView the {@link CompositionView} the title view is for
     * @param reuse - the {@link COREReuse} the composition is for.
     * @param detailedViewOn whether the detailed view should be enabled
     */
    public CompositionTitleView(CompositionView compositionView, COREReuse reuse, boolean detailedViewOn) {
        myCompositionView = compositionView;
        modelComposition = myCompositionView.getModelComposition();

        // This will be invisible and we will put expand/collapse buttons
        // inside of this container depending on the situation
        expandCollapseButtonsContainer = new RamRectangleComponent(new HorizontalLayout());
        this.addChild(expandCollapseButtonsContainer);

        RamImageComponent expandImage = new RamImageComponent(Icons.ICON_EXPAND,
                Colors.TRIANGLE_EXPAND_COLLAPSE_FILL_COLOR);
        expandImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        expandImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonExpand = new RamButton(expandImage);
        buttonExpand.setActionCommand(ACTION_COMPOSITION_SHOW_MAPPING_DETAILS);
        buttonExpand.addActionListener(this);

        RamImageComponent collapseImage = new RamImageComponent(Icons.ICON_COLLAPSE,
                Colors.TRIANGLE_EXPAND_COLLAPSE_FILL_COLOR);
        collapseImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        collapseImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonCollapse = new RamButton(collapseImage);
        buttonCollapse.setActionCommand(ACTION_COMPOSITION_HIDE_MAPPING_DETAILS);
        buttonCollapse.addActionListener(this);

        // depending on the detailed mapping view mode on/off
        if (detailedViewOn) {
            expandCollapseButtonsContainer.addChild(buttonCollapse);
        } else {
            expandCollapseButtonsContainer.addChild(buttonExpand);
        }

        RamImageComponent deleteImage = new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR);
        deleteImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        deleteImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        deleteButton = new RamButton(deleteImage);
        deleteButton.setActionCommand(ACTION_COMPOSITION_DELETE);
        deleteButton.addActionListener(this);
        deleteImage.setBufferSize(Cardinal.WEST, Fonts.FONTSIZE_COMPOSITION / 5);
        deleteImage.setBufferSize(Cardinal.EAST, Fonts.FONTSIZE_COMPOSITION / 5);
        if (detailedViewOn) {
            addChild(deleteButton);
        }

        if (reuse != null) {
            modelReuseName = new TextView(reuse, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
            modelReuseName.setUniqueName(true);
            modelReuseName.setHandler(HandlerFactory.INSTANCE.getCompositionDefaultNameHandler());
        } else {
            modelReuseName =
                    new TextView(modelComposition.getSource(), CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
            modelReuseName.setHandler(HandlerFactory.INSTANCE.getCompositionExtendNameHandler());
        }
        modelReuseName.setBufferSize(Cardinal.SOUTH, 0);
        modelReuseName.setBufferSize(Cardinal.EAST, 0);
        modelReuseName.setBufferSize(Cardinal.WEST, 0);
        modelReuseName.setFont(Fonts.FONT_COMPOSITION);
        addChild(modelReuseName);

        RamImageComponent mappingImage = new RamImageComponent(Icons.ICON_SPLIT_VIEW, Colors.ICON_STRUCT_DEFAULT_COLOR);
        mappingImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        mappingImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        mappingButton = new RamButton(mappingImage);
        mappingButton.setActionCommand(ACTION_COMPOSITION_TOGGLE_SPLIT_VIEW);
        mappingButton.addActionListener(this);
        if (detailedViewOn) {
            addChild(mappingButton);
        }

        RamImageComponent addClassifierImage = new RamImageComponent(Icons.ICON_CLASSIFIER_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        addClassifierImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        addClassifierImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonAddClassifierMapping = new RamButton(addClassifierImage);
        buttonAddClassifierMapping.setActionCommand(ACTION_CLASSIFIER_MAPPING_ADD);
        buttonAddClassifierMapping.addActionListener(this);
        if (detailedViewOn) {
            addChild(buttonAddClassifierMapping);
        }

        RamImageComponent addEnumImage = new RamImageComponent(Icons.ICON_ENUM_MAPPING_ADD,
                Colors.ICON_STRUCT_DEFAULT_COLOR);
        addEnumImage.setMinimumSize(ICON_SIZE, ICON_SIZE);
        addEnumImage.setMaximumSize(ICON_SIZE, ICON_SIZE);
        buttonAddEnumMapping = new RamButton(addEnumImage);
        buttonAddEnumMapping.setActionCommand(ACTION_ENUM_MAPPING_ADD);
        buttonAddEnumMapping.addActionListener(this);
        if (detailedViewOn) {
            addChild(buttonAddEnumMapping);
        }

        setBuffers(0);
        setLayout(new HorizontalLayoutVerticallyCentered(Fonts.FONTSIZE_COMPOSITION / 5));

    }

    @Override
    protected void destroyComponent() {
        buttonAddClassifierMapping.destroy();
        buttonAddEnumMapping.destroy();
        deleteButton.destroy();
        mappingButton.destroy();
        buttonCollapse.destroy();
        buttonExpand.destroy();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_COMPOSITION_DELETE.equals(actionCommand)) {
            myCompositionView.getCompositionContainerView().getHandler().deleteModelComposition(modelComposition);
        } else if (ACTION_COMPOSITION_SHOW_MAPPING_DETAILS.equals(actionCommand)) {
            myCompositionView.getHandler().showMappingDetails(myCompositionView);
        } else if (ACTION_COMPOSITION_HIDE_MAPPING_DETAILS.equals(actionCommand)) {
            myCompositionView.getHandler().hideMappingDetails(myCompositionView);
        } else if (ACTION_COMPOSITION_SHOW_EXTERNAL_ASPECT.equals(actionCommand)) {
            myCompositionView.getHandler().showExternalAspectOfComposition(myCompositionView);
        } else if (ACTION_COMPOSITION_TOGGLE_SPLIT_VIEW.equals(actionCommand)) {
            myCompositionView.getHandler().switchToSplitView(myCompositionView);
        } else if (ACTION_CLASSIFIER_MAPPING_ADD.equals(actionCommand)) {
            myCompositionView.getHandler().addClassifierMapping(myCompositionView);
        } else if (ACTION_ENUM_MAPPING_ADD.equals(actionCommand)) {
            myCompositionView.getHandler().addEnumMapping(myCompositionView);
        }
    }
    
    /**
     * Hides all buttons and shows the expand button.
     */
    public void hideButtons() {
        expandCollapseButtonsContainer.removeAllChildren();
        expandCollapseButtonsContainer.addChild(buttonExpand);
        
        this.removeChild(buttonAddClassifierMapping);
        this.removeChild(buttonAddEnumMapping);
        this.removeChild(deleteButton);
        this.removeChild(mappingButton);
    }
    
    /**
     * Shows all buttons including a collapse button.
     */
    public void showButtons() {
        expandCollapseButtonsContainer.removeAllChildren();
        expandCollapseButtonsContainer.addChild(buttonCollapse);
        
        this.addChild(buttonAddClassifierMapping);
        this.addChild(buttonAddEnumMapping);
        this.addChild(mappingButton);
        this.addChild(1, deleteButton);
    }
}
