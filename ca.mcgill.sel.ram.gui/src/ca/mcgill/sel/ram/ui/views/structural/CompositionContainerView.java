package ca.mcgill.sel.ram.ui.views.structural;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.INotifyChangedListener;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.COREModelReuse;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamButton;
import ca.mcgill.sel.ram.ui.components.RamExpendableComponent;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.components.RamTextComponent;
import ca.mcgill.sel.ram.ui.events.listeners.ActionListener;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayout;
import ca.mcgill.sel.ram.ui.layouts.HorizontalLayoutVerticallyCentered;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.scenes.DisplayAspectScene;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Fonts;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.structural.handler.ICompositionContainerViewHandler;
import ca.mcgill.sel.ram.util.Constants;

/**
 * An {@link CompositionContainerView} is a {@link RamExpendableComponent} which shows all the
 * model compositions that an {@link Aspect} has. By using this container it is possible to see the details of a
 * {@link COREModelComposition}.
 *
 * @author eyildirim
 * @author oalam
 */
public class CompositionContainerView extends RamExpendableComponent implements
        INotifyChangedListener, ActionListener {

    private static final String ACTION_COMPOSITION_ADD = "view.composition.add";

    private static final float ICON_SIZE = Icons.ICON_ADD.width - 8;

    // The aspect which has all the model compositions
    private Aspect aspect;

    // button for adding compositions
    private RamButton buttonPlus;

    // A container located at the top of the composition container which contains composition adding buttons for
    // classifier mappings and enum mappings. More
    // buttons can be added in the future.
    private final RamRectangleComponent buttonContainer;
    private final RamRectangleComponent titleContainer;
    private final RamRectangleComponent compositionsContainer;

    private Map<COREModelComposition, CompositionView> compositionViews;
    private boolean isModelReuseContainer;

    private ICompositionContainerViewHandler handler;

    /**
     * Constructor.
     *
     * @param displayAspectScene is the scene which has this compositionContainerView.
     * @param isModelReuseContainer is a boolean to indicate whether the CompositionsContainerView is called to show
     *            the modelreuses.
     * @param title - the title for the view
     */
    public CompositionContainerView(DisplayAspectScene displayAspectScene, boolean isModelReuseContainer,
            String title) {
        super(0, ICON_SIZE);

        this.isModelReuseContainer = isModelReuseContainer;
        aspect = displayAspectScene.getAspect();

        setNoFill(true);
        setTitleNoFill(true);
        setTitleNoStroke(true);
        setEnabled(true);
        setNoStroke(true);

        EMFEditUtil.addListenerFor(aspect, this);

        /* Build title */
        titleContainer = new RamRectangleComponent(new HorizontalLayoutVerticallyCentered());

        RamTextComponent titleText = new RamTextComponent(title);
        titleText.setFont(Fonts.INST_CONTAINER_TITLE_FONT);
        // create a row of container for the buttons and add it to the top of the menu
        buttonContainer = new RamRectangleComponent();
        buttonContainer.setLayout(new HorizontalLayout());
        initButtons();
        titleContainer.addChild(buttonContainer);
        titleContainer.addChild(titleText);
        setTitle(titleContainer);

        /* Build content */
        compositionsContainer = new RamRectangleComponent(new VerticalLayout());

        // keep a map of composition views. We used Linked hash map because we want to keep the orders that the
        // elements are added.
        compositionViews = Collections.synchronizedMap(new LinkedHashMap<COREModelComposition, CompositionView>());

        // create all composition views and add them to the container
        if (isModelReuseContainer) {
            addAllModelReuseViews();
        } else {
            addAllExtensionViews();
        }

        setHideableComponent(compositionsContainer);
        // Expanded by default
        showHideableComponent();
    }

    /**
     * Set handler for this {@link CompositionContainerView}.
     *
     * @param handler - the {@link ICompositionContainerViewHandler} to set.
     */
    public void setHandler(ICompositionContainerViewHandler handler) {
        this.handler = handler;
    }

    /**
     * Get handler for this {@link CompositionContainerView}.
     *
     * @return the {@link ICompositionContainerViewHandler} to for this view.
     */
    public ICompositionContainerViewHandler getHandler() {
        return handler;
    }

    /**
     * Getter for isModelReuseContainer.
     *
     * @return isModelReuseContainer - whether this container is to show model reuses.
     */
    public boolean isModelReuseContainer() {
        return isModelReuseContainer;
    }

    /**
     * Adds the plus button.
     */
    public void initButtons() {
        buttonContainer.removeAllChildren();
        // add plus button to the top row button container
        RamImageComponent addImage = new RamImageComponent(Icons.ICON_ADD, Colors.ICON_STRUCT_DEFAULT_COLOR,
                ICON_SIZE, ICON_SIZE);

        buttonPlus = new RamButton(addImage);
        buttonPlus.setActionCommand(ACTION_COMPOSITION_ADD);
        buttonPlus.addActionListener(this);

        buttonContainer.addChild(buttonPlus);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();

        if (ACTION_COMPOSITION_ADD.equals(actionCommand)) {
            if (isModelReuseContainer && aspect.getRealizes().isEmpty()) {
                RamApp.getActiveScene().displayPopup(Strings.popupAspectNeedsRealize(aspect.getName()),
                        PopupType.ERROR);
            } else {
                getHandler().loadBrowser(aspect);
            }
        } else {
            super.actionPerformed(event);
        }
    }

    /**
     * Adds all extension views.
     */
    private void addAllExtensionViews() {
        for (COREModelExtension extension : aspect.getModelExtensions()) {
            createCompositionView(extension, null);
        }
    }

    /**
     * Adds all ModelReuseViews.
     */
    private void addAllModelReuseViews() {
        for (COREModelReuse modelReuse : aspect.getModelReuses()) {
            if (!isAssociationReuse(modelReuse)) {
                createCompositionView(modelReuse, modelReuse.getReuse());
            }
        }
    }

    /**
     * Creates a composition view and put composition data into the map along with its mapping view. It also adds
     * itself as a child.
     *
     * @param composition composition to be created
     * @return composition just created.
     */
    private CompositionView createCompositionView(COREModelComposition composition) {
        return createCompositionView(composition, null);
    }

    /**
     * Creates a composition view and puts composition data into the map along with its mapping view.
     * It also adds itself as a child.
     *
     * @param composition the composition to create a view for
     * @param reuse the reuse the composition is for
     * @return the composition view that was created
     */
    private CompositionView createCompositionView(COREModelComposition composition, COREReuse reuse) {
        CompositionView view = new CompositionView(composition, reuse, this);
        compositionViews.put(composition, view);
        compositionsContainer.addChild(view);
        return view;
    }

    /**
     * Deletes a composition view.
     *
     * @param modelComposition is the model composition to be deleted.
     */
    private void deleteCompositionView(COREModelComposition modelComposition) {
        CompositionView view = compositionViews.get(modelComposition);
        compositionViews.remove(modelComposition);
        compositionsContainer.removeChild(view);
        view.destroy();
    }

    @Override
    public void destroy() {
        EMFEditUtil.removeListenerFor(aspect, this);
        super.destroy();
    }

    /**
     * Gets the button container of the view.
     *
     * @return buttonContainer
     */
    public RamRectangleComponent getButtonContainer() {
        return buttonContainer;
    }

    /**
     * Gets the map for the composition views.
     *
     * @return the map for all Compositions to InstantiatoinViews.
     */
    public Map<COREModelComposition, CompositionView> getCompositionViews() {
        return compositionViews;
    }

    /**
     * Gets the plus button.
     *
     * @return plus button
     */
    public RamButton getPlusButton() {
        return buttonPlus;
    }

    /**
     * Gets the visible biggest container of the {@link CompositionContainerView}.
     *
     * @return biggest container of the {@link CompositionContainerView}.
     */
    public RamRectangleComponent getCompositionsContainer() {
        return compositionsContainer;
    }

    @Override
    public void notifyChanged(Notification notification) {

        if (notification.getNotifier() == aspect) {

            if (notification.getFeature() == CorePackage.Literals.CORE_MODEL__MODEL_REUSES && isModelReuseContainer) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREModelReuse modelReuseToAdd = (COREModelReuse) notification.getNewValue();
                        if (!isAssociationReuse(modelReuseToAdd)) {
                            createCompositionView(modelReuseToAdd, modelReuseToAdd.getReuse());
                        }
                        break;
                    case Notification.REMOVE:
                        COREModelReuse modelReuseToRemove = (COREModelReuse) notification.getOldValue();
                        if (!isAssociationReuse(modelReuseToRemove)) {
                            deleteCompositionView(modelReuseToRemove);
                        }
                        break;
                }
            } else if (notification.getFeature() == CorePackage.Literals.CORE_MODEL__MODEL_EXTENSIONS
                    && !isModelReuseContainer) {
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        COREModelExtension compositionNew = (COREModelExtension) notification.getNewValue();
                        createCompositionView(compositionNew);
                        break;

                    case Notification.REMOVE:
                        COREModelExtension compositionOld = (COREModelExtension) notification.getOldValue();
                        deleteCompositionView(compositionOld);
                        break;
                }
            }
        }
    }

    /**
     * Returns whether if the model reuse is a reuse of the association concern.
     *
     * @param modelReuse The model reuse to check for.
     * @return true if the model reuse is a reuse of the association concern.
     */
    private static boolean isAssociationReuse(COREModelReuse modelReuse) {
        return Constants.ASSOCIATION_CONCERN_NAME.equals(modelReuse.getReuse().getReusedConcern().getName());
    }
}
