package ca.mcgill.sel.ram.ui.views.message.handler.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldEvent;

import ca.mcgill.sel.commons.emf.util.EMFModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Gate;
import ca.mcgill.sel.ram.Interaction;
import ca.mcgill.sel.ram.InteractionFragment;
import ca.mcgill.sel.ram.Message;
import ca.mcgill.sel.ram.MessageSort;
import ca.mcgill.sel.ram.Operation;
import ca.mcgill.sel.ram.RamPackage;
import ca.mcgill.sel.ram.controller.ControllerFactory;
import ca.mcgill.sel.ram.controller.MessageViewController;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamSelectorComponent;
import ca.mcgill.sel.ram.ui.components.listeners.AbstractDefaultRamSelectorListener;
import ca.mcgill.sel.ram.ui.events.listeners.ITapAndHoldListener;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView;
import ca.mcgill.sel.ram.ui.views.OptionSelectorView.Iconified;
import ca.mcgill.sel.ram.ui.views.handler.BaseHandler;
import ca.mcgill.sel.ram.ui.views.message.MessageCallView;
import ca.mcgill.sel.ram.ui.views.message.MessageViewView;
import ca.mcgill.sel.ram.util.MessageViewUtil;

/**
 * The default handler for {@link ca.mcgill.sel.ram.Message}s.
 * 
 * @author mschoettle
 */
public class MessageHandler extends BaseHandler implements ITapAndHoldListener {

    /**
     * The options to display for a message call.
     */
    private enum MessageOptions implements Iconified {
        MOVE_UP(new RamImageComponent(Icons.ICON_MOVE_UP, Colors.ICON_ARROW_COLOR)), 
        MOVE_DOWN(new RamImageComponent(Icons.ICON_MOVE_DOWN, Colors.ICON_ARROW_COLOR)), 
        DELETE(new RamImageComponent(Icons.ICON_DELETE, Colors.ICON_DELETE_COLOR)), 
        EXPAND(new RamImageComponent(Icons.ICON_COLLAPSE, Colors.ICON_ADD_DEFAULT_COLOR)), 
        COLLAPSE(new RamImageComponent(Icons.ICON_EXPAND, Colors.ICON_DELETE_COLOR));

        private RamImageComponent icon;

        /**
         * Creates a new option literal with the given icon.
         * 
         * @param icon the icon to use for this option
         */
        MessageOptions(RamImageComponent icon) {
            this.icon = icon;
        }

        @Override
        public RamImageComponent getIcon() {
            return icon;
        }

    }

    @Override
    public boolean processTapAndHoldEvent(TapAndHoldEvent tapAndHoldEvent) {
        if (tapAndHoldEvent.isHoldComplete()) {
            MessageCallView messageCallView = (MessageCallView) tapAndHoldEvent.getTarget();
            Message message = messageCallView.getMessage();
            MessageViewView messageViewView = (MessageViewView) messageCallView.getParent().getParent();

            if (shouldProcessTapAndHold(message)) {
                final Interaction interaction = message.getInteraction();

                List<MessageOptions> options = new ArrayList<>(Arrays.asList(MessageOptions.values()));

                if (messageViewView.getCompositeSpecification().canExpandAndCollapse(message)) {
                    if (messageViewView.getCompositeSpecification().isExpanded(message)) {
                        options.remove(MessageOptions.EXPAND);
                    } else {
                        options.remove(MessageOptions.COLLAPSE);
                    }
                } else {
                    options.remove(MessageOptions.EXPAND);
                    options.remove(MessageOptions.COLLAPSE);
                }

                OptionSelectorView<MessageOptions> selector =
                        new OptionSelectorView<MessageOptions>(options);

                RamApp.getActiveScene().addComponent(selector, tapAndHoldEvent.getLocationOnScreen());

                selector.registerListener(new AbstractDefaultRamSelectorListener<MessageOptions>() {
                    @Override
                    public void elementSelected(RamSelectorComponent<MessageOptions> selector, MessageOptions element) {
                        switch (element) {
                            case DELETE:
                                MessageViewController controller =
                                        ControllerFactory.INSTANCE.getMessageViewController();
                                controller.removeMessages(interaction, message);
                                break;
                            case MOVE_UP:
                                ControllerFactory.INSTANCE.getMessageController().moveMessage(message, true);
                                break;
                            case MOVE_DOWN:
                                ControllerFactory.INSTANCE.getMessageController().moveMessage(message, false);
                                break;
                            case EXPAND:
                                expand(messageCallView);
                                break;
                            case COLLAPSE:
                                collapse(messageCallView);
                                break;
                        }
                    }
                });
            }
        }

        return true;
    }

    /**
     * Expands a message.
     * 
     * @param messageCallView The view corresponding to the message to expand
     */
    private static void expand(MessageCallView messageCallView) {
        Message message = messageCallView.getMessage();

        if (MessageViewUtil.findSpecification(message.getSignature()) == null) {
            Operation operation = message.getSignature();
            Aspect aspect = EMFModelUtil.getRootContainerOfType(operation, RamPackage.Literals.ASPECT);
            ControllerFactory.INSTANCE.getAspectController().createMessageView(aspect, operation);
        }

        MessageViewView view = (MessageViewView) messageCallView.getParent().getParent();
        view.expand(messageCallView.getMessage());
    }

    /**
     * Collapses a message.
     * 
     * @param messageCallView The view corresponding to the message to collapse
     */
    private static void collapse(MessageCallView messageCallView) {
        MessageViewView view = (MessageViewView) messageCallView.getParent().getParent();
        view.collapse(messageCallView.getMessage());
    }

    /**
     * Returns whether a tap-and-hold event should be processed.
     * An event should be processed only for those messages that are not the initial message
     * or the last reply message, i.e., the very first and last messages of a message view.
     * 
     * @param message the message on whose view the event occurred
     * @return true, whether the event should be further processed, false otherwise
     */
    private static boolean shouldProcessTapAndHold(Message message) {
        if (!(message.getSendEvent() instanceof Gate)) {
            /**
             * Make sure that only those reply messages can be handled
             * that are additional (not the last ones) and not nested behaviour ones.
             */
            InteractionFragment sendEvent = (InteractionFragment) message.getSendEvent();
            EObject interaction = message.eContainer();
            if (message.getMessageSort() != MessageSort.REPLY
                    || (sendEvent.getContainer() != interaction
                            && message.getReceiveEvent().eClass() == RamPackage.Literals.GATE)) {
                return true;
            }
        }

        return false;
    }

}
