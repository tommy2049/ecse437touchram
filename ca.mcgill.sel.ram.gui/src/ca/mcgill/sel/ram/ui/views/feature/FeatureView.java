package ca.mcgill.sel.ram.ui.views.feature;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.shapes.MTEllipse;
import org.mt4j.components.visibleComponents.shapes.MTPolygon;
import org.mt4j.input.inputProcessors.componentProcessors.AbstractComponentProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapAndHoldProcessor.TapAndHoldProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.tapProcessor.TapProcessor;
import org.mt4j.util.MTColor;

import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureRelationshipType;
import ca.mcgill.sel.core.COREReuse;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamImageComponent;
import ca.mcgill.sel.ram.ui.components.RamLineComponent;
import ca.mcgill.sel.ram.ui.components.RamRectangleComponent;
import ca.mcgill.sel.ram.ui.layouts.VerticalLayout;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.TextView;
import ca.mcgill.sel.ram.ui.views.feature.handler.IFeatureHandler;
import ca.mcgill.sel.ram.ui.views.feature.handler.impl.FeatureRelationShipHandler;
import ca.mcgill.sel.ram.ui.views.feature.handler.impl.FeatureSelectModeHandler;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature.FeatureSelectionStatus;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * Class used to represent each of the COREFeature in TouchCORE.
 * 
 * @author Nishanth
 */
public class FeatureView extends RamRectangleComponent {

    /**
     * Enum containing all the possible selection status for the features in Select mode.
     * They are associated with the color that should be given to the view.
     * 
     * @author Nishanth
     * @author CCamillieri
     */
    private enum SelectionStatus {
        /** The Feature has been selected manually. */
        SELECTED(Colors.FEATURE_SELECTED_FILL_COLOR),
        /** The Feature has been auto-selected. */
        AUTO_SELECTED(Colors.FEATURE_AUTOSELECTED_FILL_COLOR),
        /** There is an issue with the Feature selection, the feature was manually-selected. */
        WARNING_SELECTED(Colors.FEATURE_SELECTED_FILL_COLOR, Colors.FEATURE_SELECTION_CLASH_COLOR,
                Colors.FEATURE_SELECTION_CLASH_COLOR, 1),
        /** There is an issue with the Feature selection, the feature was auto-selected. */
        WARNING_AUTO_SELECTED(Colors.FEATURE_AUTOSELECTED_FILL_COLOR, Colors.FEATURE_SELECTION_CLASH_COLOR,
                Colors.FEATURE_SELECTION_CLASH_COLOR, 1),
        /** There is an issue with the Feature selection because the feature was unselected. */
        WARNING_NOT_SELECTED(Colors.FEATURE_NOT_SELECTED_FILL_COLOR, Colors.FEATURE_SELECTION_CLASH_COLOR,
                Colors.FEATURE_SELECTION_CLASH_COLOR, 1),
        /** The Feature is not selected, nor re-exposed. */
        NOT_SELECTED(Colors.FEATURE_NOT_SELECTED_FILL_COLOR),
        /** The Feature has been re-exposed. */
        RE_EXPOSED(Colors.FEATURE_REEXPOSED_FILL_COLOR);

        private MTColor fillColor;
        private MTColor strokeColor;
        private MTColor parentLinkColor;
        // Higher priority stroke color override priority of others.
        private int priority;

        /**
         * Constructor for {@link SelectionStatus}.
         * 
         * @param fillColor - the fill color of the feature when in the given status.
         */
        SelectionStatus(MTColor fillColor) {
            this(fillColor, Colors.DEFAULT_ELEMENT_STROKE_COLOR, Colors.DEFAULT_ELEMENT_STROKE_COLOR, 0);
        }

        /**
         * Constructor for {@link SelectionStatus}.
         * 
         * @param fillColor - the fill color of the feature when in the given status.
         * @param strokeColor - the stroke color of the feature when in the given status.
         * @param parentLinkColor - the color of the link to parent feature when in the given status.
         * @param priority - the priority of the stroke color for lines
         */
        SelectionStatus(MTColor fillColor, MTColor strokeColor, MTColor parentLinkColor, int priority) {
            this.fillColor = fillColor;
            this.strokeColor = strokeColor;
            this.parentLinkColor = parentLinkColor;
            this.priority = priority;
        }

        /**
         * Getter for the fill color associated with the status.
         * 
         * @return - The fill color associated with the current {@link SelectionStatus}
         */
        public MTColor getFillColor() {
            return fillColor;
        }

        /**
         * Getter for the stroke color associated with the status.
         * 
         * @return - The stroke color associated with the current {@link SelectionStatus}
         */
        public MTColor getStrokeColor() {
            return strokeColor;
        }

        /**
         * Getter for the color of the link to the parent feature associated with the status.
         * 
         * @return - The color of the link to the parentassociated with the current {@link SelectionStatus}
         */
        public MTColor getParentLinkColor() {
            return parentLinkColor;
        }

        /**
         * Getter for the priority of the feature status.
         * 
         * @return - The priority of the feature status.
         */
        public int getPriority() {
            return priority;
        }

        /**
         * Call to know if the color has to/can be changed or not.
         * 
         * @return true if there is a fill color defined
         */
        public boolean hasFillColor() {
            return fillColor != null && fillColor != Colors.FEATURE_NO_FILL;
        }

        /**
         * Get the status associated to the given {@link FeatureSelectionStatus}.
         * 
         * @param selectionStatus - The {@link FeatureSelectionStatus} of the feature.
         * @return the {@link SelectionStatus} to use to display the view.
         */
        public static SelectionStatus getSelectionStatus(FeatureSelectionStatus selectionStatus) {
            SelectionStatus status = NOT_SELECTED;
            switch (selectionStatus) {
                case NOT_SELECTED:
                    status = SelectionStatus.NOT_SELECTED;
                    break;
                case RE_EXPOSED:
                    status = SelectionStatus.RE_EXPOSED;
                    break;
                case AUTO_SELECTED:
                    status = SelectionStatus.AUTO_SELECTED;
                    break;
                case SELECTED:
                    status = SelectionStatus.SELECTED;
                    break;
                case WARNING_NOT_SELECTED:
                    status = SelectionStatus.WARNING_NOT_SELECTED;
                    break;
                case WARNING_SELECTED:
                    status = SelectionStatus.WARNING_SELECTED;
                    break;
                case WARNING_AUTO_SELECTED:
                    status = SelectionStatus.WARNING_AUTO_SELECTED;
                    break;
            }
            return status;
        }
    }

    private TextView textView;
    private MTEllipse circle;
    private float xposition;
    private float yposition;
    private List<FeatureView> childrenFeatures = new ArrayList<FeatureView>();
    private boolean isRoot;
    private float angleWithRespectToParent;
    private RamLineComponent lineToParent;

    private FeatureView parent;
    private SelectionFeature feature;

    private IFeatureHandler handler;
    private RamImageComponent image;
    private MTPolygon reuseArrow;
    private MTPolygon triangle;

    /**
     * Default constructor.
     */
    public FeatureView() {
        super(0, 0, 50, 50);
        setNoStroke(false);
        setNoFill(false);

        setAnchor(PositionAnchor.CENTER);
        setLayout(new VerticalLayout());
        setAutoMaximizes(true);
        setAutoMinimizes(true);
        setPickable(true);
    }

    /**
     * Constructor called when a Feature display is to be created.
     * 
     * @param selectionFeature - the {@link SelectionFeature} that the FeatureView will represent.
     * @param parent - the parent {@link FeatureView}, if any
     * @param type - the {@link COREFeatureRelationshipType} to the parent feature
     */
    public FeatureView(SelectionFeature selectionFeature, FeatureView parent, COREFeatureRelationshipType type) {
        this(selectionFeature, parent, type, false, true);
    }

    /**
     * Constructor called when a Feature display is to be created.
     * 
     * @param feature - the {@link SelectionFeature} that the FeatureView will represent.
     * @param parent - the parent {@link FeatureView}, if any
     * @param type - the {@link COREFeatureRelationshipType} to the parent feature
     * @param displayReuse - Whether we display the reuse name (true) or the feature name (false).
     *            If it's the reuse name, it can be updated by double-clicking on the View
     * @param editName - Whether we want to allow renaming of the name that is displayed.
     */
    public FeatureView(SelectionFeature feature, FeatureView parent, COREFeatureRelationshipType type,
            boolean displayReuse, boolean editName) {
        this();

        this.feature = feature;
        this.setParentRelationship(type);

        this.setParentFeatureView(parent);
        if (parent == null) {
            setIsRoot(true);
        }

        setBuffers(3);
        setBufferSize(Cardinal.SOUTH, 0);
        COREReuse reuse = feature.getCoreReuse();
        if (reuse != null && displayReuse) {
            textView = new TextView(reuse, CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
            textView.setUniqueName(true);
        } else {
            textView = new TextView(getFeature(), CorePackage.Literals.CORE_NAMED_ELEMENT__NAME);
            textView.setPlaceholderText(Strings.PH_ENTER_FEATURE_NAME);
        }
        if (editName) {
            textView.registerTapProcessor(HandlerFactory.INSTANCE.getTextViewHandler());
        }
        addChild(textView);

        // Set the right color for the feature.
        updateFeatureColor();

        if (!isRoot && (type == COREFeatureRelationshipType.MANDATORY
                || type == COREFeatureRelationshipType.OPTIONAL)) {
            setCircle(FeatureDiagramView.createRelationshipCircle(getWidth()));
            updateRelationshipColor();
        }
    }

    /**
     * Getter for the {@link SelectionFeature} represented by the view.
     * 
     * @return the {@link SelectionFeature} that the view represents.
     */
    public SelectionFeature getSelectionFeature() {
        return feature;
    }

    /**
     * Function to set the handler to the feature.
     * 
     * @param handler - the handler to set
     */
    public void setHandler(IFeatureHandler handler) {
        this.handler = handler;
    }

    /**
     * Function to return the Feature Handler.
     * 
     * @return handler
     */
    public IFeatureHandler getHandler() {
        return this.handler;
    }

    /**
     * Get the image associated with the view.
     * 
     * @return - The image
     */
    public RamImageComponent getImage() {
        return image;
    }

    /**
     * Set the image associated with the view.
     * 
     * @param image - The {@link RamImageComponent} to set
     */
    public void setImage(RamImageComponent image) {
        this.image = image;
    }

    /**
     * Getter for the selectionStatus attribute.
     * 
     * @return - The {@link FeatureSelectionStatus} of the view
     */
    public FeatureSelectionStatus getFeatureSelectionStatus() {
        return feature.getSelectionStatus();
    }

    /**
     * Getter for the selectionStatus. Get the value of the enum used in gui to know color of {@link FeatureView}s.
     * 
     * @return - The {@link SelectionStatus} of the view
     */
    private SelectionStatus getSelectionStatus() {
        return SelectionStatus.getSelectionStatus(feature.getSelectionStatus());
    }

    /**
     * Update for the selectionStatus attribute and its color accordingly.
     * 
     * @param selectionStatus - The new {@link SelectionStatus} of the view
     */
    public void setSelectionStatus(FeatureSelectionStatus selectionStatus) {
        feature.setStatus(selectionStatus);
        updateSelectionColor();
    }

    /**
     * Function used to set the angle of the Feature with respect to parent.
     * 
     * @param degrees - the angle to set
     */
    public void setAngleWithRespectToParent(float degrees) {
        angleWithRespectToParent = degrees;
    }

    /**
     * Function to retrieve the x position calculated.
     * 
     * @return Xposition
     */
    public float getXposition() {
        return xposition;
    }

    /**
     * Function to set the x position calculated.
     * 
     * @param xposition - position to set
     */
    public void setXposition(float xposition) {
        this.xposition = xposition;
    }

    /**
     * Function to retrieve the y position calculated.
     * 
     * @return Yposition
     */
    public float getYposition() {
        return yposition;
    }

    /**
     * Function to set the y position calculated.
     * 
     * @param yposition - position to set
     */
    public void setYposition(float yposition) {
        this.yposition = yposition;
    }

    /**
     * Function called to return the parent of the FeatureDisplay.
     * 
     * @return FeatureParent
     */
    public FeatureView getParentFeatureView() {
        return parent;
    }

    /**
     * Function called to set the parent of the FeatureDisplay.
     * 
     * @param newParent - FeatureParent
     */
    public void setParentFeatureView(FeatureView newParent) {
        this.parent = newParent;
    }

    /**
     * Function called to return all the children of the FeatureDisplay object.
     * 
     * @return children
     */
    public List<FeatureView> getChildrenFeatureViews() {
        return childrenFeatures;
    }

    /**
     * Function to check if the FeatureDisplay is the root.
     * 
     * @return isRoot
     */
    public boolean getIsRoot() {
        return isRoot;
    }

    /**
     * Function to set the current FeatureDisplay as the Root.
     * 
     * @param isRoot - root or not
     */
    public void setIsRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }

    /**
     * Function used to retrieved the COREFeature.
     * 
     * @return feature.
     */
    public COREFeature getFeature() {
        return feature.getCoreFeature();
    }

    /**
     * Function which returns the circle.
     * 
     * @return MTcomponent
     */
    public MTEllipse getRelationshipCircle() {
        return circle;
    }

    /**
     * Function to set the circle.
     * 
     * @param newCircle (MTComponent)
     */
    public void setCircle(MTEllipse newCircle) {
        if (circle != null) {
            circle.unregisterAllInputProcessors();
            circle.removeAllGestureEventListeners();
        }
        this.circle = newCircle;
    }

    /**
     * Function to get the triangle.
     * 
     * @return {@link MTPolygon}
     */
    public MTPolygon getRelationshipTriangle() {
        return triangle;
    }

    /**
     * Function to set the triangle.
     * 
     * @param newTriangle (MTPolygon)
     */
    public void setRelationshipTriangle(MTPolygon newTriangle) {
        if (triangle != null) {
            triangle.unregisterAllInputProcessors();
            triangle.removeAllGestureEventListeners();
        }

        this.triangle = newTriangle;
    }

    /**
     * Function used to get the relationship set in the Feature View.
     * 
     * @return relationship - The relationship of the Feature, XOR / OR / Null(in case of Optional/ Mandatory)
     */
    public COREFeatureRelationshipType getChildrenRelationship() {
        return feature.getChildrenRelationship();
    }

    /**
     * Function used to get the view relationship set in the Feature View. This relationship represents the Feature
     * relationship as with its parent
     * 
     * @return viewRelationship - The relationship of the Feature, XOR / OR / Mandatory / Optional
     */
    public COREFeatureRelationshipType getParentRelationship() {
        return feature.getParentRelationship();
    }

    /**
     * Function used to get the Line Component to parent.
     * 
     * @return RamLineComponent - The line component to the parent of the feature.(null if it is root).
     */
    public RamLineComponent getLineToParent() {
        return lineToParent;
    }

    /**
     * Function used to set the line of the feature to the parent.
     * 
     * @param lineToParent - The line component to the parent.
     */
    public void setLineToParent(RamLineComponent lineToParent) {
        this.lineToParent = lineToParent;
    }

    /**
     * Get the arrow to itself reuse.
     * 
     * @return - The polygon for this reuse
     */
    public MTPolygon getReuseArrow() {
        return reuseArrow;
    }

    /**
     * Associate the view with the arrow to itself reuse.
     * 
     * @param polygon - The {@link MTPolygon} representing the arrow
     */
    public void setReuseArrow(MTPolygon polygon) {
        this.reuseArrow = polygon;
    }

    /**
     * Function used to get the current position of the Feature with respect to its siblings.
     * 
     * @return position - The current position of the Feature.
     */
    public int getCurrentPosition() {
        return parent.getChildrenFeatureViews().indexOf(this);
    }

    /**
     * Function to set the relationship of the feature to its children (XOR or OR).
     * 
     * @param type - The type to be set to the feature.
     */
    public void setChildrenRelationship(COREFeatureRelationshipType type) {
        feature.setChildrenRelationShip(type);
    }

    /**
     * Function to set the relationship of the feature to the parent.
     * 
     * @param type - The type to be set to the feature.
     */
    public void setParentRelationship(COREFeatureRelationshipType type) {
        feature.setParentRelationShip(type);
    }

    /**
     * Getter for the coreReuse assoicated with the view.
     * 
     * @return the coreReuse
     */
    public COREReuse getCoreReuse() {
        return feature.getCoreReuse();
    }

    /**
     * Function to add a child.
     * 
     * @param child - The {@link FeatureView} to add as a child
     */
    public void addChild(FeatureView child) {
        getChildrenFeatureViews().add(child);
    }

    /**
     * Function used to open the keyBoard of the textView.
     */
    public void showKeyboard() {
        textView.showKeyboard();
    }
    
    /**
     * Function used to clear the name field of the textView.
     */
    public void clearNameField() {
        textView.clearText();
    }

    /*
     * --------------------- DISPLAY ------------------------------
     */
    /**
     * Set the color of the view depending of its {@link SelectionStatus}.
     */
    public void updateSelectionColor() {
        SelectionStatus selectionStatus = getSelectionStatus();
        if (selectionStatus.hasFillColor()) {
            setFillColor(selectionStatus.getFillColor());
        }
        setStrokeColor(selectionStatus.getStrokeColor());
        if (parent != null && lineToParent != null) {
            lineToParent.setStrokeColor(selectionStatus.getParentLinkColor());
            if (parent.getRelationshipTriangle() != null) {
                // Do not update line colors if another feature has higher priority
                for (FeatureView sibling : parent.getChildrenFeatureViews()) {
                    if (sibling.getSelectionStatus().getPriority() > selectionStatus.getPriority()) {
                        return;
                    }
                }
                parent.getRelationshipTriangle().setStrokeColor(selectionStatus.getParentLinkColor());
            }
        }
    }

    /**
     * Sets the fill color for the feature depending on its kind.
     */
    protected void updateFeatureColor() {
        if (feature.isReuse() || isParentReuse()) {
            if (getFeatureSelectionStatus() == FeatureSelectionStatus.RE_EXPOSED) {
                setFillColor(Colors.FEATURE_REUSE_REEXPOSE_FILL_COLOR);
            } else {
                setFillColor(Colors.FEATURE_REUSE_FILL_COLOR);
            }
        } else if (getFeature().getRealizedBy().size() > 0) {
            setFillColor(Colors.FEATURE_ASSIGNED_FILL_COLOR);
        } else if (getFeature().getRealizedBy().size() == 0) {
            setFillColor(Colors.FEATURE_UNASSIGNED_FILL_COLOR);
        }
    }

    /**
     * Highlight the feature.
     * 
     * @param highlight - whether we want to highlight the feature or not.
     */
    public void highlight(boolean highlight) {
        MTColor color = highlight ? Colors.HIGHLIGHT_ELEMENT_STROKE_COLOR : getSelectionStatus().getStrokeColor();
        this.setStrokeColor(color);
    }

    /**
     * Update the color of the views representing the relationships for the feature.
     */
    void updateRelationshipColor() {
        if (circle != null) {
            if (getParentRelationship() == COREFeatureRelationshipType.MANDATORY) {
                circle.setFillColor(Colors.FEATURE_MANDATORY_RELATIONSHIP_FILL_COLOR);
            } else if (getParentRelationship() == COREFeatureRelationshipType.OPTIONAL) {
                circle.setFillColor(Colors.FEATURE_OPTIONAL_RELATIONSHIP_FILL_COLOR);
            }
        }

        if (triangle != null) {
            if (getChildrenRelationship() == COREFeatureRelationshipType.OR) {
                triangle.setFillColor(Colors.FEATURE_OR_RELATIONSHIP_COLOR);
                triangle.setNoFill(false);
            } else if (getChildrenRelationship() == COREFeatureRelationshipType.XOR) {
                triangle.setNoFill(true);
            }
        }
    }

    /*
     * --------------------- BEHAVIOR ------------------------------
     */
    /**
     * Function to add Listeners in select mode.
     */
    public void setListenersSelectMode() {
        unregisterAllInputProcessors();
        removeAllGestureEventListeners();

        FeatureSelectModeHandler handlerCreated = HandlerFactory.INSTANCE.getFeatureSelectModeHandler();
        AbstractComponentProcessor tapProcessor = new TapProcessor(RamApp.getApplication(),
                GUIConstants.TAP_MAX_FINGER_UP, true, GUIConstants.TAP_DOUBLE_TAP_TIME);
        tapProcessor.setBubbledEventsEnabled(true);
        registerInputProcessor(tapProcessor);
        addGestureListener(TapProcessor.class, handlerCreated);

        if (!isRoot) {
            AbstractComponentProcessor tapAndHoldProcessor = new TapAndHoldProcessor(RamApp.getApplication(),
                    GUIConstants.TAP_AND_HOLD_DURATION);
            tapAndHoldProcessor.setBubbledEventsEnabled(true);
            registerInputProcessor(tapAndHoldProcessor);
            addGestureListener(TapAndHoldProcessor.class, handlerCreated);
        }
    }

    /**
     * Function to add all the listeners to to the components.
     * 
     * @param featureHandler - The handler to set for the feature
     */
    public void setListenersEditMode(IFeatureHandler featureHandler) {
        unregisterAllInputProcessors();
        removeAllGestureEventListeners();

        // TapAndHoldProcessor
        AbstractComponentProcessor tapAndHoldProcessor = new TapAndHoldProcessor(RamApp.getApplication(),
                GUIConstants.TAP_AND_HOLD_DURATION);
        tapAndHoldProcessor.setBubbledEventsEnabled(true);
        registerInputProcessor(tapAndHoldProcessor);

        if (isReuse() || isParentReuse()) {
            IFeatureHandler reuseHandler = HandlerFactory.INSTANCE.getReuseEditModeHandler();
            setHandler(reuseHandler);
            addGestureListener(TapAndHoldProcessor.class, reuseHandler);
            return;
        }

        // Set default handler
        addGestureListener(TapAndHoldProcessor.class, featureHandler);
        setHandler(featureHandler);

        AbstractComponentProcessor doubleTapProcessor = new TapProcessor(RamApp.getApplication(),
                GUIConstants.TAP_MAX_FINGER_UP, true, GUIConstants.TAP_DOUBLE_TAP_TIME);
        doubleTapProcessor.setBubbledEventsEnabled(true);
        registerInputProcessor(doubleTapProcessor);
        addGestureListener(TapProcessor.class, featureHandler);

        // Features that are not root can be dragged
        if (!isRoot) {
            AbstractComponentProcessor rightClick = new DragProcessor(RamApp.getApplication());
            rightClick.setBubbledEventsEnabled(true);
            registerInputProcessor(rightClick);
            addGestureListener(DragProcessor.class, featureHandler);
        }

        registerRelationShipListeners();
    }

    /**
     * Adds the listeners for the relationship elements.
     */
    private void registerRelationShipListeners() {
        FeatureRelationShipHandler relHandler = new FeatureRelationShipHandler();

        // Allow modification of relationship to parent (MANDATORY/OPTIONAL)
        if (circle != null) {
            circle.unregisterAllInputProcessors();
            circle.removeAllGestureEventListeners();

            circle.registerInputProcessor(new TapProcessor(RamApp.getApplication()));
            circle.addGestureListener(TapProcessor.class, relHandler);
            relHandler.setFeatureIcon(this);
        }

        // Allow modification of relationship to children (OX/XOR)
        if (triangle != null) {
            triangle.unregisterAllInputProcessors();
            triangle.removeAllGestureEventListeners();

            triangle.registerInputProcessor(new TapProcessor(RamApp.getApplication()));
            triangle.addGestureListener(TapProcessor.class, relHandler);
            relHandler.setFeatureIcon(this);
        }
    }

    /*
     * ------------------------------- HELPERS ---------------------------------
     */
    /**
     * Check if the FeatureView is the root of a reuse.
     * 
     * @return true if the feature is the root of a reuse.
     */
    public boolean isReuse() {
        return feature.isReuse();
    }

    /**
     * Check if the FeatureView comes from a reuse.
     * 
     * @return true if a parent of the feature is a reuse.
     */
    public boolean isParentReuse() {
        if (parent == null) {
            return false;
        }
        if (parent.isReuse()) {
            return true;
        }
        return parent.isParentReuse();
    }

    /**
     * Function used to get the position of a feature with respect to its parent.
     * 
     * @param degrees - At what angle the unistrokegesture was drawn.
     * @return - The position at which it should it inserted.
     */
    public int getPosition(float degrees) {

        int position = 0;
        for (FeatureView child : childrenFeatures) {
            // Only consider children that are non-reuse links,
            // because reuse links are not part of the feature model.
            if (!child.isReuse()) {
                if (degrees > child.angleWithRespectToParent) {
                    position++;
                } else {
                    break;
                }
            }
        }
        return position;
    }

    /**
     * Get the highest rank reuse parent of the given feature.
     * 
     * @return - The parentReuse, or null if not found
     */
    public FeatureView getHighestParentReuse() {
        FeatureView view = this;
        // Get closest reuse
        while (!view.isReuse() && view.getParentFeatureView() != null) {
            view = view.getParentFeatureView();
        }
        // Check if there is a higher level reuse
        if (view.getParentFeatureView() != null) {
            FeatureView higherReuse = view.getParentFeatureView().getHighestParentReuse();
            if (higherReuse != null && higherReuse.isReuse()) {
                return higherReuse;
            }
        }
        if (view.isReuse()) {
            return view;
        }
        return null;
    }

    /**
     * Function called to collect all the Features in the Feature Model. Called on root Feature of Feature Model.
     * Recursively calls on the rest of the child.
     * 
     * @return Set<COREFeature> - The collected COREFeature from the Feature Model.
     */
    public List<COREFeature> collectFeatures() {
        List<COREFeature> features = new LinkedList<COREFeature>();
        if (!isReuse()) {
            for (FeatureView child : childrenFeatures) {
                features.addAll(child.collectFeatures());
            }
            features.add(getFeature());
        }

        return features;
    }

    /**
     * Recursive function which collects all the DragContainers of itself and its children.
     * 
     * @return the list of components to be dragged
     */
    public List<MTComponent> getAllDragContainers() {
        List<MTComponent> components = new LinkedList<MTComponent>();
        for (FeatureView child : childrenFeatures) {
            if (child.lineToParent != null) {
                components.add(child.lineToParent);
            }
            if (child.reuseArrow != null) {
                components.add(child.reuseArrow);
            }
            components.addAll(child.getAllDragContainers());
            components.add(child);
        }
        if (circle != null) {
            components.add(circle);
        }
        if (triangle != null) {
            components.add(triangle);
        }
        return components;
    }

    @Override
    public void unregisterAllInputProcessors() {
        super.unregisterAllInputProcessors();
        if (circle != null) {
            circle.unregisterAllInputProcessors();
        }
        if (triangle != null) {
            triangle.unregisterAllInputProcessors();
        }
    }

    @Override
    public void removeAllGestureEventListeners() {
        super.removeAllGestureEventListeners();
        if (circle != null) {
            circle.removeAllGestureEventListeners();
        }
        if (triangle != null) {
            triangle.removeAllGestureEventListeners();
        }
    }

    @Override
    public void destroy() {
        if (circle != null) {
            circle.destroy();
        }
        if (triangle != null) {
            triangle.destroy();
        }
        if (lineToParent != null) {
            lineToParent.destroy();
        }
        if (reuseArrow != null) {
            reuseArrow.destroy();
        }
        if (image != null) {
            image.destroy();
        }
        textView.destroy();

        removeAllGestureEventListeners();
        super.destroy();
    }

}
