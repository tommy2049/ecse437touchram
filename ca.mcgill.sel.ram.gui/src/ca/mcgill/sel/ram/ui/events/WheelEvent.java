package ca.mcgill.sel.ram.ui.events;

import org.mt4j.components.interfaces.IMTComponent3D;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.util.math.Vector3D;

/**
 * An event for supporting the mouse wheel.
 * 
 * @author vbonnet
 */
public class WheelEvent extends MTGestureEvent {
    private final float scale;
    private final Vector3D scalePoint;
    
    /**
     * Creates a new wheel event.
     * 
     * @param source the processor for these kinds of events
     * @param id the ID of the event
     * @param targetComponent the component the event is performed on
     * @param scale the scale size
     * @param scalingPoint the location where the event is performed at
     */
    public WheelEvent(MouseWheelProcessor source, int id, IMTComponent3D targetComponent,
            float scale, Vector3D scalingPoint) {
        super(source, id, targetComponent);
        this.scale = scale;
        scalePoint = scalingPoint;
    }
    
    /**
     * Converts the wheel event into a scale event.
     * 
     * @return the scale event corresponding to this event
     */
    public ScaleEvent asScaleEvent() {
        final InputCursor cursor = new InputCursor();
        return new ScaleEvent(getSource(), getId(), getTarget(), cursor, cursor, scale, scale, 1, scalePoint);
    }
    
    /**
     * Converts the wheel event into a scale event with a different target.
     * 
     * @param customTarget the target that should be used for the event
     * @return the scale event corresponding to this event
     */
    public ScaleEvent asScaleEvent(IMTComponent3D customTarget) {
        final InputCursor cursor = new InputCursor();
        return new ScaleEvent(getSource(), getId(), customTarget, cursor, cursor, scale, scale, 1, scalePoint);
    }
    
    /**
     * Returns the location the event was performed at.
     * 
     * @return the location of the event
     */
    public Vector3D getLocationOnScreen() {
        return scalePoint;
    }
    
    /**
     * Returns the scale factor of the event.
     * 
     * @return the scale factor of the wheel event
     */
    public float getScale() {
        return scale;
    }
    
}
