package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.util.COREConfigurationUtil;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Handler that handles events occurring in the feature select scene when reusing a concern.
 *
 * @author CCamillieri
 */
public class BuildApplicationSceneHandler extends ConcernSelectSceneHandler {

    @Override
    public void deleteReuseFolder(DisplayConcernSelectScene scene) {
        // unused because we don't copy the concern in this mode.
    }

    @Override
    public void reuse(DisplayConcernSelectScene scene) {
        if (!scene.hasClashes() && scene.getCanReuse()) {
            try {
                COREReuseConfiguration cfg = SelectionsSingleton.getInstance().getSelectedReuseConfiguration();
                if (!COREConfigurationUtil.isConfigurationComplete(cfg)) {
                    scene.displayPopup(Strings.POPUP_ERROR_INCOMPLETE_SELECTION, PopupType.ERROR);
                    return;
                }
    
                scene.displayPopup(Strings.POPUP_REUSING);
    
                COREConcern reusedConcern = scene.getConcern();
    
                // Weave the features and rename the woven aspect
//                Aspect wovenAspect = COREWeaverUtil.createWovenAspect(cfg, reusedConcern, false, true);
//                wovenAspect.setName(reusedConcern.getName() + "Application");
    
                // Load the aspect and destroy the scene
//                scene.setTransition(new SlideTransition(RamApp.getApplication(), 700, true));
//                RamApp.getApplication().loadAspect(wovenAspect,
//                        HandlerFactory.INSTANCE.getDisplayWovenAspectSceneHandler());
//                RamApp.getApplication().destroySceneAfterTransition(scene);
                scene.displayPopup("Sorry, weaving is currently disabled");
                
                // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
            } catch (Exception e) {
                scene.displayPopup(Strings.POPUP_ERROR_REUSE, PopupType.ERROR);
                e.printStackTrace();
            }
        }
    }
}
