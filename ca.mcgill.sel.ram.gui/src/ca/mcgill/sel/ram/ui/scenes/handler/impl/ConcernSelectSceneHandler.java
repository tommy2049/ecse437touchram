package ca.mcgill.sel.ram.ui.scenes.handler.impl;

import org.eclipse.emf.ecore.EObject;
import org.mt4j.sceneManagement.transition.BlendTransition;

import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREReuseConfiguration;
import ca.mcgill.sel.core.controller.COREControllerFactory;
import ca.mcgill.sel.core.util.COREModelUtil;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.RamPopup.PopupType;
import ca.mcgill.sel.ram.ui.components.navigationbar.NavigationBar;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene;
import ca.mcgill.sel.ram.ui.scenes.DisplayConcernSelectScene.DisplayMode;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernSelectSceneHandler;
import ca.mcgill.sel.ram.ui.utils.SelectionsSingleton;
import ca.mcgill.sel.ram.ui.utils.Strings;

/**
 * Handler that handles events occurring in the feature select scene when reusing a concern.
 *
 * @author Nishanth
 * @author mschoettle
 * @author cbensoussan
 * @author oalam
 */
public class ConcernSelectSceneHandler implements IConcernSelectSceneHandler {

    /**
     * Duration for the slide transition.
     */
    private static final int TRANSITION_DURATION = 700;

    @Override
    public void switchToPreviousScene(DisplayConcernSelectScene scene) {
        SelectionsSingleton.getInstance().clearAll();
        deleteReuseFolder(scene);
        scene.unLoadAllResources();
        scene.setTransition(new BlendTransition(RamApp.getApplication(), TRANSITION_DURATION));
        scene.getPreviousScene().getCanvas().addChild(NavigationBar.getInstance());
        scene.getApplication().changeScene(scene.getPreviousScene());
        scene.getApplication().destroySceneAfterTransition(scene);
    }

    @Override
    public void deleteReuseFolder(DisplayConcernSelectScene scene) {
        // Delete folder if no other reuse uses the concern.
        COREModelUtil.deleteReusedConcernDirectory(scene.getConcern(), scene.getAspect().getCoreConcern());
    }

    @Override
    public void reuse(DisplayConcernSelectScene scene) {
        try {
            if (scene.hasClashes() || !scene.getCanReuse()) {
                return;
            }

//            scene.displayPopup(Strings.POPUP_REUSING);
//            Aspect reusingAspect = scene.getAspect();
//
//            COREConcern reusedConcern = scene.getConcern();
//
//            COREReuseConfiguration config = SelectionsSingleton.getInstance().getSelectedReuseConfiguration();
//            Aspect wovenAspect = COREWeaverUtil.createWovenAspect(config, reusedConcern, true);
//
//            if (wovenAspect != null) {
//                createModelReuse(reusingAspect, wovenAspect, reusedConcern);
//            }
//
//          
//            SelectionsSingleton.getInstance().clearAll();
//            switchToPreviousScene(scene);
            scene.displayPopup("Sorry, weaving is currently disabled");

            // CHECKSTYLE:IGNORE IllegalCatch: Many exceptions can occur and we don't want to crash the application.
        } catch (Exception e) {
            scene.displayPopup(Strings.POPUP_ERROR_REUSE, PopupType.ERROR);
            e.printStackTrace();
        }
    }

    /**
     * Creates the model reuse from the reused concern and the given woven model.
     * 
     * @param aspect The reusing aspect.
     * @param wovenAspect The woven aspect.
     * @param concern The reused concern.
     */
    public void createModelReuse(Aspect aspect, Aspect wovenAspect, COREConcern concern) {
        COREReuseConfiguration configuration = SelectionsSingleton.getInstance().getSelectedReuseConfiguration();
        COREConcern reusingConcern = aspect.getCoreConcern();
        
        COREControllerFactory.INSTANCE.getReuseController()
                .createModelReuse(aspect, reusingConcern, concern, wovenAspect, configuration);
    }

    @Override
    public void clear(DisplayConcernSelectScene scene) {
        // clear selections
        SelectionsSingleton.getInstance().resetSelection();
        scene.redrawFeatureDiagram(true);
    }

    @Override
    public void switchMode(DisplayConcernSelectScene scene) {
        if (scene.getCurrentMode().equals(DisplayMode.NEXT)) {
            scene.setCurrentMode(DisplayMode.FULL);
        } else if (scene.getCurrentMode().equals(DisplayMode.FULL)) {
            scene.setCurrentMode(DisplayMode.NEXT);
        }
        scene.updateModeButtons();
        scene.redrawFeatureDiagram(true);
    }

    @Override
    public void save(EObject element) {
        // unused
    }

    @Override
    public void undo(EObject element) {
        // unused
    }

    @Override
    public void redo(EObject element) {
        // unused
    }
}
