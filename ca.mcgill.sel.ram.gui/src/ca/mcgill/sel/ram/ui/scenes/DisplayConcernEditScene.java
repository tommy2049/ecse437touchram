package ca.mcgill.sel.ram.ui.scenes;

import java.io.File;
import java.util.Map.Entry;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;

import ca.mcgill.sel.commons.emf.util.EMFEditUtil;
import ca.mcgill.sel.core.COREConcern;
import ca.mcgill.sel.core.COREFeature;
import ca.mcgill.sel.core.COREFeatureModel;
import ca.mcgill.sel.core.COREImpactModel;
import ca.mcgill.sel.core.COREImpactNode;
import ca.mcgill.sel.core.CorePackage;
import ca.mcgill.sel.core.impl.LayoutContainerMapImpl;
import ca.mcgill.sel.ram.ui.RamApp;
import ca.mcgill.sel.ram.ui.components.ConfirmPopup;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent.HorizontalStick;
import ca.mcgill.sel.ram.ui.components.RamPanelComponent.VerticalStick;
import ca.mcgill.sel.ram.ui.components.browser.AspectFileBrowser;
import ca.mcgill.sel.ram.ui.scenes.handler.IConcernEditSceneHandler;
import ca.mcgill.sel.ram.ui.utils.Colors;
import ca.mcgill.sel.ram.ui.utils.GUIConstants;
import ca.mcgill.sel.ram.ui.utils.Icons;
import ca.mcgill.sel.ram.ui.utils.Strings;
import ca.mcgill.sel.ram.ui.views.containers.COREConstraintContainer;
import ca.mcgill.sel.ram.ui.views.containers.COREFeatureModelLegendPanel;
import ca.mcgill.sel.ram.ui.views.containers.COREImpactConcernEditContainer;
import ca.mcgill.sel.ram.ui.views.feature.FeatureDiagramEditView;
import ca.mcgill.sel.ram.ui.views.feature.FeatureView;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint;
import ca.mcgill.sel.ram.ui.views.feature.helpers.Constraint.ConstraintType;
import ca.mcgill.sel.ram.ui.views.feature.helpers.SelectionFeature;
import ca.mcgill.sel.ram.ui.views.handler.HandlerFactory;

/**
 * Class for the FeatureModel scene of the concern.
 *
 * @author Nishanth
 */
public class DisplayConcernEditScene extends AbstractConcernScene<FeatureDiagramEditView, IConcernEditSceneHandler>
        implements INotifyChangedListener {

    private static final String ACTION_MENU = "display.menu";
    private static final String ACTION_NEW_ASPECT = "new.aspect";
    private static final String ACTION_NEW_IMPACT_MODEL = "new.impact.model";
    private static final String ACTION_SHOW_HIDE_REUSE = "show.hide.reuse";
    private static final String ACTION_SHOW_HIDE_REEXPOSED = "show.hide.reexposed";
    private static final String ACTION_EXPAND_FEATURES = "expand.feature";
    
    // name for sub menues
    private static final String SUBMENU_REUSES = "sub.reuses";

    /**
     * Boolean value to hold whether the reuses should be displayed or not.
     */
    private boolean showReuses;
    /**
     * Boolean value to hold whether the reexposed features from reuses should be displayed or not.
     */
    private boolean showReexposed;
    
    private COREConstraintContainer constraintContainer;
    private COREImpactConcernEditContainer impactEditContainer;

    /**
     * Constructor called when the concern is loaded. Initializes everything and adds to the TopLayer.
     *
     * @param app - The current {@link RamApp}
     * @param concern - The {@link COREConcern} to display
     */
    public DisplayConcernEditScene(RamApp app, COREConcern concern) {

        // Calling the constructor of the Abstract scene with the name of the concern
        super(app, concern.getName(), true);
        
        // Assigning the concern and the filePath
        this.concern = concern;
        filePath = new File(concern.eResource().getURI().trimSegments(1).toFileString());

        concernRectangle = new MTRectangle(app, getWidth(), getHeight());
        concernRectangle.setFillColor(Colors.BACKGROUND_COLOR);
        concernRectangle.setNoFill(false);
        concernRectangle.setPickable(false);
        concernRectangle.unregisterAllInputProcessors();
       
        EMFEditUtil.addListenerFor(concern.getFeatureModel(), this);
        EMFEditUtil.addListenerFor(concern, this);
        COREImpactModel im = concern.getImpactModel();
        
        if (im != null) {
            EMFEditUtil.addListenerFor(im, this);
        }

        build();
        
        setCommandStackListener(concern);

        navbar.pushSection(concern.getName(),  navbar.getCoreNamer(), concern);
        navbar.setCurrentConcern(concern);
    }

    /**
     * Build the view.
     */
    @Override
    protected void build() {
        // Get the root of the feature model
        COREFeatureModel fm = concern.getFeatureModel();
        root = fm.getRoot();
        
        // Draw feature diagram
        redrawFeatureDiagram(true);
    
        // Container for the constraints defined in the concern
        constraintContainer = new COREConstraintContainer(HorizontalStick.RIGHT, VerticalStick.BOTTOM, true);
        constraintContainer.setElements(getConstraints(false));
        
        // Container for the goals defined in the concern's impact model
        impactEditContainer = new COREImpactConcernEditContainer(concern, this);
        
        COREFeatureModelLegendPanel legend =
                new COREFeatureModelLegendPanel(HorizontalStick.CENTER, VerticalStick.BOTTOM);
    
        containerLayer.addChild(legend);
        //containerLayer.addChild(aspectContainer);
        containerLayer.addChild(constraintContainer);
        containerLayer.addChild(impactEditContainer);
    }

    @Override
    public boolean destroy() {
        EMFEditUtil.removeListenerFor(concern.getFeatureModel(), this);
        EMFEditUtil.removeListenerFor(concern, this);
        COREImpactModel im = concern.getImpactModel();
        if (im != null) {
            EMFEditUtil.removeListenerFor(im, this);
        }
        boolean ret = super.destroy();

        return ret;
    }

    /**
     * Getter for the showReuse boolean.
     *
     * @return showReuses
     */
    public boolean isShowReuses() {
        return showReuses;
    }

    /**
     * Shows a confirm popup for the given aspect to ask the user whether the aspect should be saved.
     *
     * @param parent the scene where the popup should be displayed, usually the current scene
     * @param listener the listener to inform which option the user selected
     */
    public void showCloseConfirmPopup(RamAbstractScene<?> parent, ConfirmPopup.SelectionListener listener) {
        String message = Strings.MODEL_CONCERN + " " + concern.getName() + Strings.POPUP_MODIFIED_SAVE;
        ConfirmPopup saveConfirmPopup = new ConfirmPopup(message, ConfirmPopup.OptionType.YES_NO_CANCEL);
        saveConfirmPopup.setListener(listener);
        
        parent.displayPopup(saveConfirmPopup);
    }

    @Override
    public void onEnter() {
        AspectFileBrowser.setInitialFolder(filePath.getAbsolutePath());
        AspectFileBrowser.setRootFolder(filePath.getAbsolutePath());
    }

    /*
     * --------------------- DISPLAY -------------------
     */
    /**
     * Redraw only the feature diagram.
     */
    @Override
    public void redrawFeatureDiagram(boolean repopulate) {
        // Keep the same position and scaling as before
        if (featureDiagramView == null) {
            featureDiagramView = new FeatureDiagramEditView(getWidth(), getHeight(), this);
            containerLayer.addChild(featureDiagramView);
            featureDiagramView.setHandler(HandlerFactory.INSTANCE.getFeatureDiagramEditHandler());
        } else {
            featureDiagramView.updateFeaturesDisplay(repopulate);
        }
    }

    /*
     * --------------------- BEHAVIOR ---------------------
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        String actionCommand = event.getActionCommand();
        if (ACTION_MENU.equals(actionCommand)) {
            handler.switchToHome(this);
        } else if (ACTION_NEW_ASPECT.equals(actionCommand)) {
            handler.createAspect(this);
        } else if (ACTION_NEW_IMPACT_MODEL.equals(actionCommand)) {
            handler.createImpactModel(this);
        } else if (ACTION_SHOW_HIDE_REUSE.equals(actionCommand)) {
            switchShowHideReuse();
        } else if (ACTION_SHOW_HIDE_REEXPOSED.equals(actionCommand)) {
            switchShowHideReexposed();
        } else if (ACTION_EXPAND_FEATURES.equals(actionCommand)) {
            featureDiagramView.expandAllFeatures();
        } else {
            super.actionPerformed(event);
        }
    }

    @Override
    public void notifyChanged(Notification notification) {
        if (notification.getFeature() == CorePackage.Literals.CORE_CONCERN__IMPACT_MODEL) {
            COREImpactModel im;
            switch (notification.getEventType()) {
                case Notification.SET:
                    im = (COREImpactModel) notification.getNewValue();
                    if (im != null) {
                        EMFEditUtil.addListenerFor(im, this);
                        if (impactEditContainer != null) {
                            impactEditContainer.setImpactModel(im);
                        }
                        break;
                    }
                case Notification.UNSET:
                    im = (COREImpactModel) notification.getOldValue();
                    EMFEditUtil.removeListenerFor(im, this);
                    break;
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_IMPACT_MODEL__LAYOUTS) {
            if (this == RamApp.getActiveScene()) {
                final LayoutContainerMapImpl containerMapImpl;
                switch (notification.getEventType()) {
                    case Notification.ADD:
                        containerMapImpl = (LayoutContainerMapImpl) notification.getNewValue();

                        containerMapImpl.eAdapters().add(new EContentAdapter() {
                            @Override
                            public void notifyChanged(final Notification notif) {
                                if (notif.getFeature() == CorePackage.Literals.LAYOUT_CONTAINER_MAP__VALUE) {
                                    final Entry<?, ?> entry;
                                    switch (notif.getEventType()) {
                                        case Notification.ADD:
                                            containerMapImpl.eAdapters().remove(this);
                                            entry = (Entry<?, ?>) notif.getNewValue();
                                            final COREImpactModel im = concern.getImpactModel();

                                            RamApp.getApplication().invokeLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    getApplication().displayEditImpactModel(concern, im,
                                                            concern.getName(), (COREImpactNode) entry.getKey());
                                                }
                                            });
                                            break;
                                    }
                                }
                            }
                        });
                        break;
                }
            }
        } else if (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__EXCLUDES
                || notification.getFeature() == CorePackage.Literals.CORE_FEATURE__REQUIRES) {
            ConstraintType type = (notification.getFeature() == CorePackage.Literals.CORE_FEATURE__EXCLUDES)
                    ? ConstraintType.EXCLUDES : ConstraintType.REQUIRES;
            SelectionFeature root = featureDiagramView.getRootFeature().getSelectionFeature();
            SelectionFeature owner =
                    SelectionFeature.findSelectionFeature((COREFeature) notification.getNotifier(), root);
            SelectionFeature target;
            switch (notification.getEventType()) {
                case Notification.ADD:
                    target = SelectionFeature.findSelectionFeature((COREFeature) notification.getNewValue(), root);
                    constraintContainer.addElement(new Constraint(owner, target, type));
                    break;
                case Notification.REMOVE:
                    target = SelectionFeature.findSelectionFeature((COREFeature) notification.getOldValue(), root);
                    constraintContainer.removeElement(new Constraint(owner, target, type));
                    break;
            }
        }
    }

    /**
     * Function called when the show// hide reuse button need to be interchanged and switched accordingly.
     */
    public void switchShowHideReuse() {
        if (!showReuses) {
            showReuses = true;
            menu.toggleAction(showReuses, ACTION_SHOW_HIDE_REUSE);
            // Add button for re-exposed features
            menu.addAction(Strings.MENU_SHOW_REEXPOSED, Strings.MENU_HIDE_REEXPOSED,
                    Icons.ICON_MENU_SHOW_REEXPOSED, Icons.ICON_MENU_HIDE_REEEXPOSED, ACTION_SHOW_HIDE_REEXPOSED, this,
                    SUBMENU_REUSES, true, false);
        } else {
            showReuses = false;
            menu.toggleAction(showReuses, ACTION_SHOW_HIDE_REUSE);
            menu.removeAction(ACTION_SHOW_HIDE_REEXPOSED);
        }
        featureDiagramView.switchShowReuses();
    }

    /**
     * Function called when the show// hide reuse button need to be interchanged and switched accordingly.
     */
    private void switchShowHideReexposed() {
        if (!showReuses) {
            return;
        }
        showReexposed = !showReexposed;
        menu.toggleAction(showReexposed, ACTION_SHOW_HIDE_REEXPOSED);

        featureDiagramView.switchShowReexposed();
    }

    /*
     * ---------------------- COLLAPSE -----------------------------
     */
    /**
     * Update the status of the 'expand all' button.
     * Check if features have to be removed from the collapsed feature list and if so remove them.
     */
    public void updateExpandButton() {
        // Remove features that can no longer be collapsed
        featureDiagramView.checkCollapseValidity();
        menu.enableAction(featureDiagramView.hasCollapsedElements(true), ACTION_EXPAND_FEATURES);
    }

    /**
     * Hide features in the view.
     * Children feature of the given feature are collapsed and will not be displayed
     *
     * @param view - The {@link FeatureView} to hide
     */
    public void switchCollapse(FeatureView view) {
        featureDiagramView.switchCollapse(view);
    }

    @Override
    protected void initMenu() {
        //this.getMenu().addAction(Strings.MENU_HOME, Icons.ICON_MENU_HOME, ACTION_MENU, this, true);
        this.getMenu().addSubMenu(1, GUIConstants.MENU_EXTRA);
        this.getMenu().addAction(Strings.MENU_NEW_ASPECT, Icons.ICON_MENU_ADD_ASPECT, ACTION_NEW_ASPECT, this,
                GUIConstants.MENU_EXTRA,
                true);
        this.getMenu().addAction(Strings.MENU_NEW_IMPACT_ROOT, Icons.ICON_MENU_NEW_ROOT_GOAL, ACTION_NEW_IMPACT_MODEL,
                this, GUIConstants.MENU_EXTRA, true);
        // Reuses
        this.getMenu().addSubMenu(1, SUBMENU_REUSES);
        // Expand
        this.getMenu().addAction(Strings.MENU_EXPAND_ALL, Icons.ICON_MENU_EXPAND_ALL_FEATURES, ACTION_EXPAND_FEATURES,
                this, SUBMENU_REUSES, true);
        this.getMenu().enableAction(false, ACTION_EXPAND_FEATURES);

        this.getMenu().addAction(Strings.MENU_SHOW_REUSES, Strings.MENU_HIDE_REUSES, Icons.ICON_MENU_SHOW_REUSE,
                Icons.ICON_MENU_HIDE_REUSE, ACTION_SHOW_HIDE_REUSE, this, SUBMENU_REUSES, true, false);
       
    }
 
    @Override
    protected EObject getElementToSave() {
        return concern;
    }

}
