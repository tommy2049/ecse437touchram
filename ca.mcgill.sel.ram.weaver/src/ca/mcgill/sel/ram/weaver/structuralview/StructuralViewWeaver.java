package ca.mcgill.sel.ram.weaver.structuralview;

import java.util.Iterator;
import java.util.List;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelExtension;
import ca.mcgill.sel.core.weaver.util.COREModelElementReferenceUpdater;
import ca.mcgill.sel.core.weaver.util.COREReferenceUpdater;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;
import ca.mcgill.sel.core.weaver.COREWeaver;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Class;
import ca.mcgill.sel.ram.Classifier;

/**
 * This is the class that represents our structural view weaver.
 * @author walabe
 *
 */
public final class StructuralViewWeaver {
    
    /**
     * The singleton instance.
     */
    private static StructuralViewWeaver instance;
            
    /**
     * Our Constructor it initializes our attributes.
     */
    private StructuralViewWeaver() {
    }
    
    /**
     * Returns the singleton instance of the {@link StructuralViewWeaver}.
     * 
     * @return the singleton instance
     */
    public static StructuralViewWeaver getInstance() {
        if (instance == null) {
            instance = new StructuralViewWeaver();
        }
        
        return instance;
    }
    
    /**
     * The main method of our class structural view weaver
     * that is called to perform weaving for a given instantiation.
     * @param base The aspect we want to weave into
     * @param composition The COREModelComposition to the aspect we want to weave into base
     * @param weavingInfo the weaving information that needs to be updated
     * @return The newly woven aspect
     */
    public Aspect weaveModel(Aspect base, COREModelComposition composition, WeavingInformation weavingInfo) {
        // Reset the class variables this is important
        // for weaveAll when we perform recursive weaving
//        isExtends = false;
//        
//        weavingInformation = new RAMWeavingInformation();
//        
//        Aspect baseCopy;
//        Aspect externalAspect = (Aspect) composition.getSource();
//        
//        // 1. Perform the pre-processing phase and populating weavingInformation data-structure.
//        isExtends = PreProcessor.performPreProcess(composition, base, weavingInformation);
//        // 2. Perform the weaving phase.
//        baseCopy = weave(base, externalAspect);
//        // 3. Perform the post-processing phase and update all the references.
//        //PostProcessor.performUpdates(base, externalAspect, weavingInformation, composition);
        
        // return the new woven base with the lower level aspect woven into it.
                
        Aspect sourceAspect = (Aspect) composition.getSource();

        // First go through all classifiers in the source model, and either merge or copy them into the
        // base model
        for (Iterator<Classifier> iterator = sourceAspect.getStructuralView().getClasses().iterator(); iterator
                .hasNext(); ) {
            Classifier classFromSource = iterator.next();
            
            System.out.print("Handling " + classFromSource.getName() + ", which is ");
            
            if (weavingInfo.wasWoven(classFromSource)) {
                List<Classifier> mappedToBaseClasses = (List<Classifier>) weavingInfo.getToElements(classFromSource);
              
                System.out.println(" mapped to: " + mappedToBaseClasses);
                // merge class
                for (Classifier baseTargetClass : mappedToBaseClasses) {
                    if (baseTargetClass instanceof Class && classFromSource instanceof Class) {
                        AttributesWeaver.weaveAttributes((Class) baseTargetClass, (Class) classFromSource, weavingInfo);
                        OperationsWeaver.weaveOperations((Class) baseTargetClass, (Class) classFromSource,
                                base, composition instanceof COREModelExtension, weavingInfo);
                        
                        // Weave inheritance
                        // Although this code will set the supertype reference to point to the super class in
                        // the source model, the reference updater will take care of changing the reference to
                        // the class in the base model eventually
                        for (Classifier parent : classFromSource.getSuperTypes()) {                    
                            if (!baseTargetClass.getSuperTypes().contains(parent)) {
                                baseTargetClass.getSuperTypes().add(parent);
                            }
                        }
                    }
                }
                

            } else {
                // there is no user-defined mapping, therefore we simply copy the model element from
                // the source model into the base
                System.out.println("not mapped");
                Classifier modelElementCopy = COREWeaver.copyModelElement(classFromSource, base,
                        weavingInfo, composition);
                base.getStructuralView().getClasses().add(modelElementCopy);               
            }
        }
        
        // now update any references that still refer to the external source model
        COREReferenceUpdater updater = COREModelElementReferenceUpdater.getInstance();
        updater.update(base.getStructuralView(), weavingInfo);
        return base;
    }
    
//    /**
//     * Private helper weave method is called after all pre-processing is performed.
//     * @param target The aspect we want to weave lowerLevelAspect into
//     * @param source The aspect that is woven into higherLevelAspect
//     * @return The newly Woven aspect
//     */
//    private Aspect weave(final Aspect target, final Aspect source) {
//        
//        // loop through all the classes of the target model and figure out which ones
//        // need to be merged since they where mapped to by a class in the source aspect
//        for (Classifier classFromSource : source.getStructuralView().getClasses()) {
//            // next find the class that mapped to this clss in the lower level
//            // aspect
//            for (Classifier classFromHigherLevel : weavingInformation.getToElements(classFromSource)) {
//                // now perform the weaving
//                // TODO this will not work if implementation classes can be
//                // mapped to regular classes
//                // First weave in the attributes
//                if (classFromSource instanceof Class && classFromHigherLevel instanceof Class) {
//                    AttributesWeaver.weaveAttributes((Class) classFromHigherLevel, (Class) classFromSource,
//                            weavingInformation);
//                }
//                // weaving of associations and operations is common to both classes and implementation classes
//                // AssociationsWeaver.weaveAssociations(classFromHigherLevel, classFromLowerLevel,
//                //        higherLevelAspect, weavingInformation);
//                
//                OperationsWeaver.weaveOperations(classFromHigherLevel, classFromSource,
//                        target, isExtends, weavingInformation);
//                
//                // Weave inheritance.
//                for (Classifier parent : classFromSource.getSuperTypes()) {                    
//                    if (!classFromHigherLevel.getSuperTypes().contains(parent)) {
//                        classFromHigherLevel.getSuperTypes().add(parent);
//                    }
//                }
//            }
//        }
//        
//        AssociationsWeaver.weaveAssociations(target, source, weavingInformation);
//        
//        return target;
//    }
//    
}
