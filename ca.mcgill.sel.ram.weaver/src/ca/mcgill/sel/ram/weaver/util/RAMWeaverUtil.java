package ca.mcgill.sel.ram.weaver.util;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

import ca.mcgill.sel.core.COREModelComposition;
import ca.mcgill.sel.core.COREModelElement;
import ca.mcgill.sel.core.COREModelElementComposition;
import ca.mcgill.sel.core.weaver.util.WeavingInformation;
import ca.mcgill.sel.ram.Aspect;
import ca.mcgill.sel.ram.Traceable;
import ca.mcgill.sel.ram.util.RAMModelUtil;

/**
 * Helper class with convenient static methods for working with the {@link ca.mcgill.sel.ram.weaver.RAMWeaver}.
 *
 * @author ccamilieri
 */
public final class RAMWeaverUtil {

    /**
     * Creates a new instance of {@link RAMWeaverUtil}.
     */
    private RAMWeaverUtil() {
        // suppress default constructor
    }

    /**
     * Get {@link WeavingInformation} that maps the elements of the two given aspects.
     * This is done by looking at the highest level origin elements in both aspects.
     *
     * @param from - The aspect that will populate the 'froms' of the {@link WeavingInformation}
     * @param to - The aspect that will populate the 'tos' of the {@link WeavingInformation}
     * @return {@link WeavingInformation} containing mappings of form "base woven model" -> "new woven model"
     */
    private static WeavingInformation getWeavingInformationFromTrace(Aspect from, Aspect to) {
        WeavingInformation weavingInformation = new RAMWeavingInformation();

        // Go through the aspect hierarchy
        TreeIterator<EObject> iterator = EcoreUtil.getAllContents(from, true);

        while (iterator.hasNext()) {
            EObject wovenElement = iterator.next();
            if (wovenElement instanceof Traceable) {
                Traceable fromElement = (Traceable) wovenElement;
                EObject origin = RAMModelUtil.getOriginElement(fromElement);
                if (origin != null) {
                    COREModelElement toElement = RAMModelUtil.getWovenElementFromOrigin(to, (COREModelElement) origin);
                    if (toElement != null) {
                        weavingInformation.add(fromElement, toElement);
                    }
                }
            }
        }

        return weavingInformation;
    }

    /**
     * This method finds two duplicate instantiations and then merges them.
     * There should be at most one duplicate instantiation after each
     * WeaveSingle.
     * 
     * @param base is the Aspect which instantiations are being merged
     */
    public static void mergeCompositions(Aspect base) {
        COREModelComposition comp1 = null;
        COREModelComposition comp2 = null;
        int loop = 0;
        outerloop: while (loop < base.getModelExtensions().size()) {
            for (int j = loop + 1; j < base.getModelExtensions().size(); j++) {
                if (base.getModelExtensions().get(loop).getSource() == base.getModelExtensions().get(j).getSource()) {
                    comp1 = base.getModelExtensions().get(loop);
                    comp2 = base.getModelExtensions().get(j);
                    break outerloop;
                }
            }
            loop++;
        }
        if (comp1 != null && comp2 != null) {
            mergeCompositions(base, comp1, comp2);
            // Recursively restart again to ensure other duplicates are merged as well.
            // See issue #433.
            mergeCompositions(base);
        }
    }

    /**
     * Merges two compositions.
     * 
     * @param base aspect that contains the compositions
     * @param toKeep is the composition into which the result of merging will be saved
     * @param toDelete is the composition that is merged into toKeep and then deleted
     */
    private static void mergeCompositions(Aspect base, COREModelComposition toKeep,
            COREModelComposition toDelete) {
        // Add the compositions from toMerge to the compositions of the toKeep.
        toKeep.getCompositions().addAll(toDelete.getCompositions());
        base.getModelExtensions().remove(toDelete);
    }

}
